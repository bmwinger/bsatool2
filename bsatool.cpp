#include <filesystem>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

#include <argparse/argparse.hpp>

#include <components/bsa/bsa_file.hpp>
#include <components/misc/stringops.hpp>

struct Arguments
{
    std::string mode;
    std::string filename;
    std::string extractfile;
    std::string folder;
    std::string outdir;

    Bsa::BSAFile::ArchiveType type;
    bool compressed;
    bool share;
    bool longformat;
    bool fullpath;
};

Bsa::BSAFile::ArchiveType toArchiveType(const std::string& type)
{
    if (type == "tes3")
        return Bsa::BSAFile::ArchiveType::TES3;
    if(type == "tes4")
        return Bsa::BSAFile::ArchiveType::TES4;
    if(type == "fo3" ||type == "fnv" || type == "tes5")
        return Bsa::BSAFile::ArchiveType::FO3;
    if(type == "sse")
        return Bsa::BSAFile::ArchiveType::SSE;
    if(type == "fo4")
        return Bsa::BSAFile::ArchiveType::FO4;
    if(type == "fo4dds")
        return Bsa::BSAFile::ArchiveType::FO4dds;
    throw std::runtime_error("Unknow archive type " + type);
}

void parseSubcommand(std::string subcommand, argparse::ArgumentParser &parser)
{

}

bool parseOptions (int argc, char** argv, Arguments &info)
{
    argparse::ArgumentParser program("bsatool2", BSATOOL_VERSION);
    program.add_description("Inspect and extract files from Bethesda BSA archives\n\n"
            "Usages:\n"
            "  bsatool list [-l] archivefile\n"
            "      List the files presents in the input archive.\n\n"
            "  bsatool extract [-f] archivefile [file_to_extract] [output_directory]\n"
            "      Extract a file from the input archive.\n\n"
            "  bsatool extractall archivefile [output_directory]\n"
            "      Extract all files from the input archive.\n\n"
            "  bsatool create archivefile -t type folder [-z] [-s]\n"
            "      Create an archive with files from folder.\n\n"
            "      Types:\n"
            "      - tes3       Morrowind archive format\n"
            "      - tes4       Oblivion archive format\n"
            "      - tes5       Skyrim LE archive format(fo3 / fnv / tes5 are technically the same)\n"
            "      - fo3        Fallout 3 archive format\n"
            "      - fnv        Fallout : New Vegas archive format\n"
            "      - sse        Skyrim Special Edition archive format\n"
            "      - fo4        Fallout 4 General archive format\n"
            "      - fo4dds     Fallout 4 DDS archive format(streamed DDS textures mipmaps)\n"
            "      Parameters:\n"
            "      - s          Share identic files in the archive.\n"
            "      - z          Compress archive..Keep in mind that sounds and voices\n"
            "      don't work in compressed archives in all Bethesda games!\n"
            "      Even if your archive contains a single sound / voice file\n"
            "      out of thousands, it must be uncompressed.\n"
            );
    program.add_argument("mode");
    program.add_argument("<subcommand_args...>").nargs(argparse::nargs_pattern::any);

    argparse::ArgumentParser listParser("bsatool2 list", "", argparse::default_arguments::help);
    listParser.add_argument("archiveFile");
    listParser.add_argument("-l", "--long")
      .help("Include extra information in archive listing.")
      .default_value(false)
      .implicit_value(true);

    argparse::ArgumentParser extractParser("bsatool2 extract", "", argparse::default_arguments::help);
    extractParser.add_argument("archiveFile");
    extractParser.add_argument("file_to_extract");
    extractParser.add_argument("output_directory");
    extractParser.add_argument("-f", "--full-path")
      .help("Create directory hierarchy on file extraction (always true for extractall)")
      .default_value(false)
      .implicit_value(true);

    argparse::ArgumentParser extractAllParser("bsatool2 extractall", "", argparse::default_arguments::help);
    extractAllParser.add_argument("archiveFile");
    extractAllParser.add_argument("output_directory");

    argparse::ArgumentParser createParser("bsatool2 create", "", argparse::default_arguments::help);
    createParser.add_argument("archiveFile");
    createParser.add_argument("folder");
    createParser.add_argument("-t", "--type")
      .help("Type of archive to create, could be: tes3, tes4, fo3, fnv, tes5, sse, fo4, fo4dds.");
    createParser.add_argument("-z", "--compressed")
      .help("Create a compressed archive.")
      .default_value(false)
      .implicit_value(true);
    createParser.add_argument("-s", "--share")
      .help("Identical files are not duplicated in the archive.")
      .default_value(false)
      .implicit_value(true);

    try
    {
        program.parse_args(std::min(argc, 2), argv);
    }
    catch(std::exception &e)
    {
        std::cout << "ERROR parsing arguments: " << e.what() << "\n\n"
            << program << std::endl;
        return false;
    }
    auto mode = program.get("mode");
    auto parser = program;
    if (mode == "list")
        parser = listParser;
    else if (mode == "extract")
        parser = extractParser;
    else if (mode == "extractall")
        parser = extractAllParser;
    else if (mode == "create")
        parser = createParser;

    parseSubcommand(mode, parser);
    try
    {
        // Ignore first argument
        // so that it doesn't treat the mode as an argument to the subparser
        parser.parse_args(argc - 1, argv + 1);
    }
    catch(std::exception &e)
    {
        std::cout << "ERROR parsing arguments: " << e.what() << "\n\n"
            << parser << std::endl;
        return false;
    }

    /*
    if (variables.count ("help"))
    {
        std::cout << desc << std::endl;
        return false;
    }
    if (variables.count ("version"))
    {
        std::cout << "BSATool version " << BSATOOL_VERSION << std::endl;
        return false;
    }
    if (!variables.count("mode"))
    {
        std::cout << "ERROR: no mode specified!\n\n"
            << desc << std::endl;
        return false;
    }
    */

    info.mode = program.get("mode");
    if (!(info.mode == "list" || info.mode == "extract" || info.mode == "extractall" || info.mode == "add" || info.mode == "create"))
    {
        std::cout << std::endl << "ERROR: invalid mode \"" << info.mode << "\"\n\n"
            << program << std::endl;
        return false;
    }

    info.filename = parser.get("archiveFile");

    // Default output to the working directory
    info.outdir = ".";

    if (info.mode == "extract")
    {
        info.extractfile = parser.get("file_to_extract");
        info.outdir = parser.get("output_directory");
        info.fullpath = parser.get<bool>("full-path");
    }
    else if (info.mode == "create")
    {
        info.folder = parser.get("folder");
        info.type = toArchiveType(parser.get("type"));
        info.compressed = parser.get<bool>("compressed");
        info.share = parser.get<bool>("share");
    }
    else if (info.mode == "extractall")
    {
        info.outdir = parser.get("output_directory");
        info.fullpath = true;
    }
    else if (info.mode == "list")
        info.longformat = parser.get<bool>("long");


    return true;
}

int create(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info);
int list(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info);
int extract(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info);
int extractAll(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info);

int main(int argc, char** argv)
{
    try
    {
        Arguments info;
        if(!parseOptions (argc, argv, info))
            return 1;

        // Open file
        std::unique_ptr<Bsa::BSAFile> bsa = std::make_unique<Bsa::BSAFile>();

        if (info.mode == "create")
            return create(bsa, info);

        bsa->open(info.filename);

        if (info.mode == "list")
            return list(bsa, info);
        else if (info.mode == "extract")
            return extract(bsa, info);
        else if (info.mode == "extractall")
            return extractAll(bsa, info);
        else
        {
            std::cout << "Unsupported mode. That is not supposed to happen." << std::endl;
            return 1;
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "ERROR reading BSA archive\nDetails:\n" << e.what() << std::endl;
        return 2;
    }
}

int create(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info)
{
    typedef std::filesystem::recursive_directory_iterator directory_iterator;
    std::vector<std::string> files;
    for (directory_iterator iter(info.folder); iter != directory_iterator(); ++iter)
    {
        if (std::filesystem::is_directory(*iter))
            continue;
        auto p = iter->path().string();
        files.push_back(p.substr(info.folder.size() + 1));
    }
    bsa->create(info.filename, info.type, info.folder, files, info.compressed, info.share);

    for (const auto& file : files)
    {
        bsa->addFile(info.folder, file);
        std::cout << "Adding " << file << std::endl;
    }
    bsa->save();
    return 0;
}

int list(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info)
{
    // List all files
    const Bsa::BSAFile::FileList &files = bsa->getList();
    for (const auto& file : files)
    {
        if(info.longformat)
        {
            // Long format
            std::ios::fmtflags f(std::cout.flags());
            std::cout << std::setw(50) << std::left << file->name();
            std::cout << std::setw(8) << std::left << std::dec << file->Size;
            std::cout << "@ 0x" << std::hex << file->Offset << std::endl;
            std::cout.flags(f);
        }
        else
            std::cout << file->name() << std::endl;
    }

    return 0;
}

int extract(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info)
{
    std::string archivePath = info.extractfile;
    Misc::StringUtils::replaceAll(archivePath, "/", "\\");

    std::string extractPath = info.extractfile;
    Misc::StringUtils::replaceAll(extractPath, "\\", "/");

    if (!bsa->exists(archivePath.c_str()))
    {
        std::cout << "ERROR: file '" << archivePath << "' not found\n";
        std::cout << "In archive: " << info.filename << std::endl;
        return 3;
    }

    // Get the target path (the path the file will be extracted to)
    std::filesystem::path relPath (extractPath);
    std::filesystem::path outdir (info.outdir);

    std::filesystem::path target;
    if (info.fullpath)
        target = outdir / relPath;
    else
        target = outdir / relPath.filename();

    // Create the directory hierarchy
    std::filesystem::create_directories(target.parent_path());

    std::filesystem::file_status s = std::filesystem::status(target.parent_path());
    if (!std::filesystem::is_directory(s))
    {
        std::cout << "ERROR: " << target.parent_path() << " is not a directory." << std::endl;
        return 3;
    }

    // Get a the content of the file to extract
    auto fileContent = bsa->getFile(archivePath.c_str())->getRawData();

    std::ofstream out(target, std::ios::binary);

    // Write the file to disk
    std::cout << "Extracting " << info.extractfile << " to " << target << std::endl;

    out.write(fileContent.data(), fileContent.size());

    return 0;
}

int extractAll(std::unique_ptr<Bsa::BSAFile>& bsa, Arguments& info)
{
    for (const auto &file : bsa->getList())
    {
        std::string extractPath(file->name());
        Misc::StringUtils::replaceAll(extractPath, "\\", "/");

        // Get the target path (the path the file will be extracted to)
        std::filesystem::path target (info.outdir);
        target /= extractPath;

        // Create the directory hierarchy
        std::filesystem::create_directories(target.parent_path());

        std::filesystem::file_status s = std::filesystem::status(target.parent_path());
        if (!std::filesystem::is_directory(s))
        {
            std::cout << "ERROR: " << target.parent_path() << " is not a directory." << std::endl;
            return 3;
        }

        // Get the content of the file to extract
        auto fileContent = bsa->getFile(file)->getRawData();
        std::ofstream out(target, std::ios::binary);

        // Write the file to disk
        std::cout << "Extracting " << target << std::endl;
        out.write(fileContent.data(), fileContent.size());
    }

    return 0;
}
