/*
  OpenMW - The completely unofficial reimplementation of Morrowind
  Copyright (C) 2008-2010  Nicolay Korslund
  Email: < korslund@gmail.com >
  WWW: https://openmw.org/

  This file (bsa_file.cpp) is part of the OpenMW package.

  OpenMW is distributed as free software: you can redistribute it
  and/or modify it under the terms of the GNU General Public License
  version 3, as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  version 3 along with this program. If not, see
  https://www.gnu.org/licenses/ .

 */

#include "bsa_file.hpp"
#include "wbBSArchive.h"
#include <components/bsa/memorystream.hpp>

namespace Bsa
{

BSAFile::BSAFile() : mArchive(std::make_unique<wbBSArchive::TwbBSArchive>())
{}

BSAFile::~BSAFile()
{}

void BSAFile::create(const std::string& archive, ArchiveType type, const std::string& rootDir, std::vector<std::string>& files, bool compressed, bool share)
{
    mArchive->Compress() = compressed;
    mArchive->ShareData() = share;
    mArchive->CreateArchive(archive, wbBSArchive::TBSArchiveType(type), files);
}

void BSAFile::addFile(const std::string& rootDir, const std::string& file)
{
    mArchive->AddFile(rootDir, rootDir + "/" + file);
}

/// Open an archive file.
void BSAFile::open(const std::string &file)
{
    mArchive->LoadFromFile(file);
    mFiles = mArchive->getList();
}

void BSAFile::save()
{
    mArchive->Save();
}

void BSAFile::close()
{
    mArchive->Close();
}

bool BSAFile::exists(const char* file) const
{
    return mArchive->FileExists(file);
}

IMemStreamPtr BSAFile::getFile(const char* file)
{
    FileStruct* aFileRecord = (FileStruct*)mArchive->FindFileRecord(file);
    if (!aFileRecord)
        throw std::runtime_error("File " + std::string(file) + " not found in archive " + mArchive->FileName());
    return getFile(aFileRecord);
}

IMemStreamPtr BSAFile::getFile(const FileStruct* file)
{
    return std::make_shared<Bsa::MemoryInputStream>(mArchive->ExtractFileData((wbBSArchive::Pointer)file));
}

const std::string& BSAFile::getFilename() const
{
    return mArchive->FileName();
}

}
