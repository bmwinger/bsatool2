/*
  OpenMW - The completely unofficial reimplementation of Morrowind
  Copyright (C) 2008-2010  Nicolay Korslund
  Email: < korslund@gmail.com >
  WWW: https://openmw.org/

  This file (bsa_file.h) is part of the OpenMW package.

  OpenMW is distributed as free software: you can redistribute it
  and/or modify it under the terms of the GNU General Public License
  version 3, as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  version 3 along with this program. If not, see
  https://www.gnu.org/licenses/ .

 */

#ifndef BSA_BSA_FILE_H
#define BSA_BSA_FILE_H

#include <cstdint>
#include <string>
#include <vector>
#include <map>

#include <components/misc/stringops.hpp>

#include <components/bsa/memorystream.hpp>

namespace wbBSArchive
{
    class TwbBSArchive;
}

namespace Bsa
{

/**
   This class is used to read "Bethesda Archive Files", or BSAs.
 */
class BSAFile
{
public:

    enum class ArchiveType { None, TES3, TES4, FO3, SSE, FO4, FO4dds };

    /// Represents one file entry in the archive
    struct FileStruct {
        virtual ~FileStruct() {}
        uint32_t Size = 0;
        int64_t Offset = 0;
        std::string Name;
        virtual const std::string& name() const = 0;   //the full path name
    };
    typedef std::vector<const FileStruct*> FileList;
protected:
    FileList mFiles;
    std::unique_ptr<wbBSArchive::TwbBSArchive> mArchive;
public:
    /* -----------------------------------
     * BSA management methods
     * -----------------------------------
     */

    BSAFile();

    ~BSAFile();

    void create(const std::string& archive, ArchiveType type, const std::string& rootDir, std::vector<std::string>& files, bool compressed = false, bool share = false);

    void addFile(const std::string& rootDir, const std::string& file);

    /// Open an archive file.
    void open(const std::string &file);

    void save();

    void close();

    /* -----------------------------------
     * Archive file routines
     * -----------------------------------
     */

    /// Check if a file exists
    bool exists(const char* file) const;

    /** Open a file contained in the archive. Throws an exception if the
        file doesn't exist.
     * @note Thread safe.
    */
    IMemStreamPtr getFile(const char *file);

    /** Open a file contained in the archive.
     * @note Thread safe.
    */
    IMemStreamPtr getFile(const FileStruct* file);

    /// Get a list of all files
    /// @note Thread safe.
    const FileList &getList() const
    { return mFiles; }

    const std::string& getFilename() const;
};

}

#endif
