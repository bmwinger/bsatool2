/*******************************************************************************

The contents of this file are subject to the Mozilla Public License
Version 1.1 (the "License"); you may not use this file except in
compliance with the License.You may obtain a copy of the License at
http ://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS"
basis, WITHOUT WARRANTY OF ANY KIND, either express or implied.See the
License for the specific language governing rightsand limitations
under the License.

*******************************************************************************/

#include <filesystem>
#include <fstream>

#include <components/bsa/bsa_file.hpp>
#include <components/files/constrainedfilestream.hpp>
#include "zstr.hpp"
#include <lz4frame.h>
#include <array>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <numeric>

#include <openssl/md5.h>

namespace wbBSArchive {

namespace detail {

template <class T, std::size_t N, std::size_t... I>
constexpr std::array<std::remove_cv_t<T>, N>
  to_array_impl(T (&a)[N], std::index_sequence<I...>)
{
  return { {a[I]...} };
}

}

template <class T, std::size_t N>
constexpr std::array<std::remove_cv_t<T>, N> to_array(T (&a)[N])
{
  return detail::to_array_impl(a, std::make_index_sequence<N>{});
}


  using Cardinal = uint32_t;
  using PCardinal = Cardinal*;
  using Integer = int32_t;
  using UInt64 = uint64_t;
  using Int64 = int64_t;
  using Word = uint16_t;
  using Byte = uint8_t;
  using PByte = Byte*;
  using Pointer = void*;
  using TBytes = std::vector<char>;

  using TMagic4 = std::array<char, 4>;

  enum TDXGI {
    DXGI_FORMAT_UNKNOWN,
    DXGI_FORMAT_R32G32B32A32_TYPELESS,
    DXGI_FORMAT_R32G32B32A32_FLOAT,
    DXGI_FORMAT_R32G32B32A32_UINT,
    DXGI_FORMAT_R32G32B32A32_SINT,
    DXGI_FORMAT_R32G32B32_TYPELESS,
    DXGI_FORMAT_R32G32B32_FLOAT,
    DXGI_FORMAT_R32G32B32_UINT,
    DXGI_FORMAT_R32G32B32_SINT,
    DXGI_FORMAT_R16G16B16A16_TYPELESS,
    DXGI_FORMAT_R16G16B16A16_FLOAT,
    DXGI_FORMAT_R16G16B16A16_UNORM,
    DXGI_FORMAT_R16G16B16A16_UINT,
    DXGI_FORMAT_R16G16B16A16_SNORM,
    DXGI_FORMAT_R16G16B16A16_SINT,
    DXGI_FORMAT_R32G32_TYPELESS,
    DXGI_FORMAT_R32G32_FLOAT,
    DXGI_FORMAT_R32G32_UINT,
    DXGI_FORMAT_R32G32_SINT,
    DXGI_FORMAT_R32G8X24_TYPELESS,
    DXGI_FORMAT_D32_FLOAT_S8X24_UINT,
    DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS,
    DXGI_FORMAT_X32_TYPELESS_G8X24_UINT,
    DXGI_FORMAT_R10G10B10A2_TYPELESS,
    DXGI_FORMAT_R10G10B10A2_UNORM,
    DXGI_FORMAT_R10G10B10A2_UINT,
    DXGI_FORMAT_R11G11B10_FLOAT,
    DXGI_FORMAT_R8G8B8A8_TYPELESS,
    DXGI_FORMAT_R8G8B8A8_UNORM,
    DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
    DXGI_FORMAT_R8G8B8A8_UINT,
    DXGI_FORMAT_R8G8B8A8_SNORM,
    DXGI_FORMAT_R8G8B8A8_SINT,
    DXGI_FORMAT_R16G16_TYPELESS,
    DXGI_FORMAT_R16G16_FLOAT,
    DXGI_FORMAT_R16G16_UNORM,
    DXGI_FORMAT_R16G16_UINT,
    DXGI_FORMAT_R16G16_SNORM,
    DXGI_FORMAT_R16G16_SINT,
    DXGI_FORMAT_R32_TYPELESS,
    DXGI_FORMAT_D32_FLOAT,
    DXGI_FORMAT_R32_FLOAT,
    DXGI_FORMAT_R32_UINT,
    DXGI_FORMAT_R32_SINT,
    DXGI_FORMAT_R24G8_TYPELESS,
    DXGI_FORMAT_D24_UNORM_S8_UINT,
    DXGI_FORMAT_R24_UNORM_X8_TYPELESS,
    DXGI_FORMAT_X24_TYPELESS_G8_UINT,
    DXGI_FORMAT_R8G8_TYPELESS,
    DXGI_FORMAT_R8G8_UNORM,
    DXGI_FORMAT_R8G8_UINT,
    DXGI_FORMAT_R8G8_SNORM,
    DXGI_FORMAT_R8G8_SINT,
    DXGI_FORMAT_R16_TYPELESS,
    DXGI_FORMAT_R16_FLOAT,
    DXGI_FORMAT_D16_UNORM,
    DXGI_FORMAT_R16_UNORM,
    DXGI_FORMAT_R16_UINT,
    DXGI_FORMAT_R16_SNORM,
    DXGI_FORMAT_R16_SINT,
    DXGI_FORMAT_R8_TYPELESS,
    DXGI_FORMAT_R8_UNORM,
    DXGI_FORMAT_R8_UINT,
    DXGI_FORMAT_R8_SNORM,
    DXGI_FORMAT_R8_SINT,
    DXGI_FORMAT_A8_UNORM,
    DXGI_FORMAT_R1_UNORM,
    DXGI_FORMAT_R9G9B9E5_SHAREDEXP,
    DXGI_FORMAT_R8G8_B8G8_UNORM,
    DXGI_FORMAT_G8R8_G8B8_UNORM,
    DXGI_FORMAT_BC1_TYPELESS,
    DXGI_FORMAT_BC1_UNORM,
    DXGI_FORMAT_BC1_UNORM_SRGB,
    DXGI_FORMAT_BC2_TYPELESS,
    DXGI_FORMAT_BC2_UNORM,
    DXGI_FORMAT_BC2_UNORM_SRGB,
    DXGI_FORMAT_BC3_TYPELESS,
    DXGI_FORMAT_BC3_UNORM,
    DXGI_FORMAT_BC3_UNORM_SRGB,
    DXGI_FORMAT_BC4_TYPELESS,
    DXGI_FORMAT_BC4_UNORM,
    DXGI_FORMAT_BC4_SNORM,
    DXGI_FORMAT_BC5_TYPELESS,
    DXGI_FORMAT_BC5_UNORM,
    DXGI_FORMAT_BC5_SNORM,
    DXGI_FORMAT_B5G6R5_UNORM,
    DXGI_FORMAT_B5G5R5A1_UNORM,
    DXGI_FORMAT_B8G8R8A8_UNORM,
    DXGI_FORMAT_B8G8R8X8_UNORM,
    DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM,
    DXGI_FORMAT_B8G8R8A8_TYPELESS,
    DXGI_FORMAT_B8G8R8A8_UNORM_SRGB,
    DXGI_FORMAT_B8G8R8X8_TYPELESS,
    DXGI_FORMAT_B8G8R8X8_UNORM_SRGB,
    DXGI_FORMAT_BC6H_TYPELESS,
    DXGI_FORMAT_BC6H_UF16,
    DXGI_FORMAT_BC6H_SF16,
    DXGI_FORMAT_BC7_TYPELESS,
    DXGI_FORMAT_BC7_UNORM,
    DXGI_FORMAT_BC7_UNORM_SRGB,
    DXGI_FORMAT_AYUV,
    DXGI_FORMAT_Y410,
    DXGI_FORMAT_Y416,
    DXGI_FORMAT_NV12,
    DXGI_FORMAT_P010,
    DXGI_FORMAT_P016,
    DXGI_FORMAT_420_OPAQUE,
    DXGI_FORMAT_YUY2,
    DXGI_FORMAT_Y210,
    DXGI_FORMAT_Y216,
    DXGI_FORMAT_NV11,
    DXGI_FORMAT_AI44,
    DXGI_FORMAT_IA44,
    DXGI_FORMAT_P8,
    DXGI_FORMAT_A8P8,
    DXGI_FORMAT_B4G4R4A4_UNORM,
    DXGI_FORMAT_P208,
    DXGI_FORMAT_V208,
    DXGI_FORMAT_V408
  };

#pragma pack(push)
#pragma pack(1)
  struct TDDSHeader {
    char Magic[4];
    Cardinal dwSize = 0;
    Cardinal dwFlags = 0;
    Cardinal dwHeight = 0;
    Cardinal dwWidth = 0;
    Cardinal dwPitchOrLinearSize = 0;
    Cardinal dwDepth = 0;
    Cardinal dwMipMapCount = 0;
    Cardinal dwReserved1[11];
    struct {
      Cardinal dwSize;
      Cardinal dwFlags;
      char dwFourCC[4];
      Cardinal dwRGBBitCount;
      Cardinal dwRBitMask;
      Cardinal dwGBitMask;
      Cardinal dwBBitMask;
      Cardinal dwABitMask;
    } ddspf;
    Cardinal dwCaps;
    Cardinal dwCaps2;
    Cardinal dwCaps3;
    Cardinal dwCaps4;
    Cardinal dwReserved2;
  };
#pragma pack(pop)
  using PDDSHeader = TDDSHeader*;

#pragma pack(push)
#pragma pack(1)
  struct TDDSHeaderDX10 {
    Integer dxgiFormat;
    Cardinal resourceDimension;
    Cardinal miscFlags;
    Cardinal arraySize;
    Cardinal miscFlags2;
  };
#pragma pack(pop)
  using PDDSHeaderDX10 = TDDSHeaderDX10*;

  class TwbBSArchive;

  enum TBSArchiveType { baNone, baTES3, baTES4, baFO3, baSSE, baFO4, baFO4dds };
  enum TBSArchiveState { stReading = 0x1, stWriting=0x2 };
  using TBSArchiveStates = Byte; //set of TBSArchiveState;

  using TBSFileIterationProc = std::function<bool(const TwbBSArchive& aArchive, const std::string& aFileName,
    Pointer aFileRecord, Pointer aFolderRecord)>;

  struct TDDSInfo { Integer Width, Height, MipMaps; };
  using TBSFileDDSInfoProc = std::function<void (const TwbBSArchive& aArchive, const std::string& aFileName,
    TDDSInfo& aInfo)>;

#pragma pack(push)
#pragma pack(1)
  struct TwbBSHeaderTES3 {
    Cardinal HashOffset = 0;
    Cardinal FileCount = 0;
  };
#pragma pack(pop)

  struct TwbBSFileTES3 : Bsa::BSAFile::FileStruct {
    UInt64 Hash = 0;
    const std::string & name() const override { return Name; }
  };
  using PwbBSFileTES3 = TwbBSFileTES3*;

#pragma pack(push)
#pragma pack(1)
  struct TwbBSHeaderTES4 {
    Cardinal FoldersOffset = 0;
    Cardinal Flags = 0;
    Cardinal FolderCount = 0;
    Cardinal FileCount = 0;
    Cardinal FolderNamesLength = 0;
    Cardinal FileNamesLength = 0;
    Cardinal FileFlags = 0;
  };
#pragma pack(pop)

  struct TwbBSFileTES4 : Bsa::BSAFile::FileStruct {
    UInt64 Hash = 0;
    auto Compressed(const TwbBSArchive& bsa) const -> bool;
    auto RawSize() const -> Cardinal;
    const std::string& name() const override { return FullName; }
    std::string FullName;
  };
  using PwbBSFileTES4 = TwbBSFileTES4*;

  struct TwbBSFolderTES4 {
    UInt64 Hash = 0;
    Cardinal FileCount = 0;
    Cardinal Unk32 = 0;
    Int64 Offset = 0;
    std::string Name;
    std::vector<TwbBSFileTES4> Files;
  };
  using PwbBSFolderTES4 = TwbBSFolderTES4*;

#pragma pack(push)
#pragma pack(1)
  struct TwbBSHeaderFO4 {
    char Magic[4];
    Cardinal FileCount = 0;
    Int64 FileTableOffset = 0;
  };
#pragma pack(pop)

  struct TwbBSTexChunkRec {
    Cardinal Size = 0;
    Cardinal PackedSize = 0;
    Int64 Offset = 0;
    Word StartMip = 0;
    Word EndMip = 0;
  };
  using PwbBSTexChunkRec = TwbBSTexChunkRec*;

  struct TwbBSFileFO4 : Bsa::BSAFile::FileStruct {
    Cardinal NameHash = 0;
    TMagic4 Ext;
    Cardinal DirHash = 0;
    // GNRL archive format
    Cardinal Unknown = 0;
    Cardinal PackedSize = 0;
    //
    // DX10 archive format
    Byte UnknownTex = 0;
    //ChunkHeaderSize: Word;
    Word Height = 0;
    Word Width = 0;
    Byte NumMips = 0;
    Byte DXGIFormat = 0;
    Word CubeMaps = 0;
    std::vector<TwbBSTexChunkRec> TexChunks;
    //
    //std::string DXGIFormatName() const;
    const std::string& name() const override { return Name; }
  };
  using PwbBSFileFO4 = TwbBSFileFO4*;

  using TPackedDataHash = std::array<Byte, 16>;
  struct TPackedDataInfo {
    Cardinal Size = 0;
    TPackedDataHash Hash;
    Pointer FileRecord = nullptr;
  };
  using PPackedDataInfo = TPackedDataInfo*;

  class TwbBSArchive {
  private:
    std::fstream fStream;
    TBSArchiveStates fStates = 0;
    TBSArchiveType fType = baNone;
    std::string fFileName;
    TMagic4 fMagic;
    Cardinal fVersion = 0;
    bool fCompress = false;
    bool fShareData = false;
    TBSFileDDSInfoProc fDDSInfoProc = {};

    TwbBSHeaderTES3 fHeaderTES3 = {};
    std::vector<TwbBSFileTES3> fFilesTES3;

    TwbBSHeaderTES4 fHeaderTES4 = {};
    std::vector<TwbBSFolderTES4> fFoldersTES4;

    TwbBSHeaderFO4 fHeaderFO4 = {};
    std::vector<TwbBSFileFO4> fFilesFO4;
    Integer fMaxChunkCount = 0;
    Integer fSingleMipChunkX = 0;
    Integer fSingleMipChunkY = 0;
  public:
    Int64 fDataOffset = 0;
  private:
    std::vector<TPackedDataInfo> fPackedData;
    //Integer fPackedDataCount;

    std::string GetArchiveFormatName() const;
    Cardinal GetFileCount() const;
    void SetArchiveFlags(Cardinal aFlags);
    bool FindFileRecordTES3(const std::string& aFileName, Integer& aFileIdx) const;
    bool FindFileRecordTES4(const std::string& aFileName, Integer& aFolderIdx, Integer& aFileIdx) const;
    bool FindFileRecordFO4(const std::string& aFileName, Integer&  aFileIdx) const;
    Integer GetDDSMipChunkNum(TDDSInfo& aDDSInfo) const;
    TPackedDataHash CalcDataHash(const char* aData, Cardinal aLen) const;
    bool FindPackedData(Cardinal aSize, const TPackedDataHash& aHash, Pointer aFileRecord);
    void AddPackedData(Cardinal aSize, const TPackedDataHash& aHash, Pointer aFileRecord);

  public:
    //Sync: IReadWriteSync; [TODO]
    TwbBSArchive();
    ~TwbBSArchive();
    void LoadFromFile(const std::string& aFileName);
    void CreateArchive(const std::string& aFileName, TBSArchiveType aType,
        std::vector<std::string>& aFilesList);
    void Save();
    void AddFile(const std::string& aRootDir, const std::string& aFileName);
    void AddFile(const std::string& aFileName, const TBytes& aData);
    Pointer FindFileRecord(const std::string& aFileName);
    TBytes ExtractFileData(Pointer aFileRecord);
    TBytes ExtractFileData(const std::string& aFileName);
    void ExtractFileToStream(const std::string& aFileName, std::ostream& aStream);
    void ExtractFile(const std::string& aFileName, const std::string& aSaveAs);
    void IterateFiles(const TBSFileIterationProc& aProc);
    bool FileExists(const std::string& aFileName);
    void ResourceList(std::vector<std::string>& aList, const std::string& aFolder = "") const;
    void ResolveHash(const UInt64 aHash, std::vector<std::string>& Results);
    //void IterateFolders(const TBSFileIterationProcaProc& aProc);
    void Close();

    const std::string& FileName() const { return fFileName; }
    TBSArchiveType ArchiveType() const { return fType; }
    Cardinal Version() const { return fVersion; }
    std::string FormatName() const { return GetArchiveFormatName(); }
    Cardinal FileCount() const { return GetFileCount(); }

    Cardinal GetArchiveFlags() const { return fHeaderTES4.Flags; }

    Cardinal& FileFlags() { return fHeaderTES4.FileFlags; }
    const Cardinal& FileFlags() const { return fHeaderTES4.FileFlags; }

    bool& Compress() { return fCompress; }
    const bool& Compress() const { return fCompress; }

    bool& ShareData() { return fShareData; }
    const bool& ShareData() const { return fShareData; }

    TBSFileDDSInfoProc& DDSInfoProc() { return fDDSInfoProc; }
    const TBSFileDDSInfoProc& DDSInfoProc() const { return fDDSInfoProc; }

    Bsa::BSAFile::FileList getList() const;
  };

size_t SplitDirName(const std::string& aFileName, std::string& Dir, std::string& Name);
size_t SplitNameExt(const std::string& aFileName, std::string& Name, std::string& Ext);
UInt64 CreateHashTES3(const std::string& aFileName);
UInt64 CreateHashTES4(const std::string& aFileName);
Cardinal CreateHashFO4(const std::string& aFileName);




  constexpr const std::array TBSArchiveFormatName = {
    "None",
    "Morrowind",
    "Oblivion",
    "Fallout 3, New Vegas, Skyrim LE",
    "Skyrim Special Edition",
    "Fallout 4 General",
    "Fallout 4 DDS"
  };

  constexpr const TMagic4 MAGIC_TES3 = {'\x0', '\x1', '\x0', '\x0'};
  constexpr const TMagic4 MAGIC_BSA  = {'B', 'S', 'A', '\x0'};
  constexpr const TMagic4 MAGIC_BTDX = {'B', 'T', 'D', 'X'};
  constexpr const TMagic4 MAGIC_GNRL = {'G', 'N', 'R', 'L'};
  constexpr const TMagic4 MAGIC_DX10 = {'D', 'X', '1', '0'};
  constexpr const TMagic4 MAGIC_DDS  = {'D', 'D', 'S', ' '};
  constexpr const TMagic4 MAGIC_DXT1 = {'D', 'X', 'T', '1'};
  constexpr const TMagic4 MAGIC_DXT3 = {'D', 'X', 'T', '3'};
  constexpr const TMagic4 MAGIC_DXT5 = {'D', 'X', 'T', '5'};
  constexpr const TMagic4 MAGIC_ATI1 = {'A', 'T', 'I', '1'};
  constexpr const TMagic4 MAGIC_ATI2 = {'A', 'T', 'I', '2'};
  constexpr const TMagic4 MAGIC_BC4S = {'B', 'C', '4', 'S'};
  constexpr const TMagic4 MAGIC_BC4U = {'B', 'C', '4', 'U'};
  constexpr const TMagic4 MAGIC_BC5S = {'B', 'C', '5', 'S'};
  constexpr const TMagic4 MAGIC_BC5U = {'B', 'C', '5', 'U'};

  constexpr const Cardinal iFileFO4Unknown = 0x00100100;
  constexpr const Cardinal iFileFO4Tail = 0xBAADF00D;



  // header versions
  constexpr const Cardinal HEADER_VERSION_TES4 = 0x67; // Oblivion
  constexpr const Cardinal HEADER_VERSION_FO3  = 0x68; // FO3, FNV, TES5
  constexpr const Cardinal HEADER_VERSION_SSE  = 0x69; // SSE
  constexpr const Cardinal HEADER_VERSION_FO4  = 0x01; // FO4

  // archive flags
  constexpr const Cardinal ARCHIVE_PATHNAMES  = 0x0001; // Whether the BSA has names for paths
  constexpr const Cardinal ARCHIVE_FILENAMES  = 0x0002; // Whether the BSA has names for files
  constexpr const Cardinal ARCHIVE_COMPRESS   = 0x0004; // Whether the files are compressed in archive (invert file's compression flag)
  constexpr const Cardinal ARCHIVE_RETAINDIR  = 0x0008;
  constexpr const Cardinal ARCHIVE_RETAINNAME = 0x0010;
  constexpr const Cardinal ARCHIVE_RETAINFOFF = 0x0020;
  constexpr const Cardinal ARCHIVE_XBOX360    = 0x0040;
  constexpr const Cardinal ARCHIVE_STARTUPSTR = 0x0080;
  constexpr const Cardinal ARCHIVE_EMBEDNAME  = 0x0100; // Whether the name is prefixed to the data
  constexpr const Cardinal ARCHIVE_XMEM       = 0x0200;
  constexpr const Cardinal ARCHIVE_UNKNOWN10  = 0x0400;

  // file flags
  constexpr const Cardinal FILE_NIF  = 0x0001;
  constexpr const Cardinal FILE_DDS  = 0x0002;
  constexpr const Cardinal FILE_XML  = 0x0004;
  constexpr const Cardinal FILE_WAV  = 0x0008;
  constexpr const Cardinal FILE_MP3  = 0x0010;
  constexpr const Cardinal FILE_TXT  = 0x0020; // TXT, HTML, BAT, SCC
  constexpr const Cardinal FILE_SPT  = 0x0040;
  constexpr const Cardinal FILE_FNT  = 0x0080; // TEX, FNT
  constexpr const Cardinal FILE_MISC = 0x0100; // CTL and others

  constexpr const Cardinal FILE_SIZE_COMPRESS = 0x40000000; // Whether the file is compressed

  constexpr const Cardinal DDSD_CAPS            = 0x00000001;
  constexpr const Cardinal DDSD_HEIGHT          = 0x00000002;
  constexpr const Cardinal DDSD_WIDTH           = 0x00000004;
  constexpr const Cardinal DDSD_PITCH           = 0x00000008;
  constexpr const Cardinal DDSD_PIXELFORMAT     = 0x00001000;
  constexpr const Cardinal DDSD_MIPMAPCOUNT     = 0x00020000;
  constexpr const Cardinal DDSD_LINEARSIZE      = 0x00080000;
  constexpr const Cardinal DDSD_DEPTH           = 0x00800000;
  constexpr const Cardinal DDSCAPS_COMPLEX      = 0x00000008;
  constexpr const Cardinal DDSCAPS_TEXTURE      = 0x00001000;
  constexpr const Cardinal DDSCAPS_MIPMAP       = 0x00400000;
  constexpr const Cardinal DDSCAPS2_CUBEMAP     = 0x00000200;
  constexpr const Cardinal DDSCAPS2_POSITIVEX   = 0x00000400;
  constexpr const Cardinal DDSCAPS2_NEGATIVEX   = 0x00000800;
  constexpr const Cardinal DDSCAPS2_POSITIVEY   = 0x00001000;
  constexpr const Cardinal DDSCAPS2_NEGATIVEY   = 0x00002000;
  constexpr const Cardinal DDSCAPS2_POSITIVEZ   = 0x00004000;
  constexpr const Cardinal DDSCAPS2_NEGATIVEZ   = 0x00008000;
  constexpr const Cardinal DDSCAPS2_VOLUME      = 0x00200000;
  constexpr const Cardinal DDPF_ALPHAPIXELS     = 0x00000001;
  constexpr const Cardinal DDPF_ALPHA           = 0x00000002;
  constexpr const Cardinal DDPF_FOURCC          = 0x00000004;
  constexpr const Cardinal DDPF_RGB             = 0x00000040;
  constexpr const Cardinal DDPF_YUV             = 0x00000200;
  constexpr const Cardinal DDPF_LUMINANCE       = 0x00020000;

  // DX10
  constexpr const Cardinal DDS_DIMENSION_TEXTURE2D       = 0x00000003;
  constexpr const Cardinal DDS_RESOURCE_MISC_TEXTURECUBE = 0x00000004;

  constexpr const Cardinal crc32table[256] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
    0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
    0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
    0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
    0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
    0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
    0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
    0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
    0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
    0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
    0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
    0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
    0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
    0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
    0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
    0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
    0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
    0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
    0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
    0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
    0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
  };

void lz4DecompressToUserBuf(char* buffer, size_t bufferSize, char* result, size_t resultSize) {
  LZ4F_decompressionContext_t context = nullptr;
  LZ4F_createDecompressionContext(&context, LZ4F_VERSION);
  LZ4F_decompressOptions_t options = {};
  LZ4F_decompress(context, result, &resultSize, buffer, &bufferSize, &options);
  LZ4F_errorCode_t errorCode = LZ4F_freeDecompressionContext(context);
  if (LZ4F_isError(errorCode))
    throw std::runtime_error("LZ4 decompression error: " + std::string(LZ4F_getErrorName(errorCode)));
}

void DecompressToUserBuf(char* Buffer, size_t BufferSize, char* Result, size_t ResultSize) {
  std::stringbuf inputStreamBuf;
  zstr::istreambuf zStreamBuf(&inputStreamBuf);
  inputStreamBuf.sputn(Buffer, BufferSize);
  zStreamBuf.sgetn(Result, ResultSize);
}

void lz4CompressStream(std::istream& aSource, std::ostream& aCompressed, int aCompressionLevel = 8) {
  constexpr const auto LZ4_BLOCKSIZEID_DEFAULT = 7;
  auto blockSize = 1 << (8 + 2 * LZ4_BLOCKSIZEID_DEFAULT);

  LZ4F_compressionContext_t context = nullptr;
  LZ4F_createCompressionContext(&context, LZ4F_VERSION);

  LZ4F_preferences_t prefs = {};
  prefs.autoFlush = 1;
  prefs.compressionLevel = 8;
  prefs.frameInfo.blockMode = LZ4F_blockMode_t(1);
  prefs.frameInfo.blockSizeID = LZ4F_blockSizeID_t(LZ4_BLOCKSIZEID_DEFAULT);
  prefs.frameInfo.contentChecksumFlag = LZ4F_contentChecksum_t(1);

  TBytes inBuff(blockSize);
  TBytes outBuff(LZ4F_compressBound(blockSize, &prefs));

  auto headerSize = LZ4F_compressBegin(context, outBuff.data(), outBuff.size(), &prefs);
  aCompressed.write(outBuff.data(), headerSize);

  aSource.read(inBuff.data(), blockSize);
  auto readSize = aSource.gcount();
  while (readSize > 0) {
    auto outSize = LZ4F_compressUpdate(context, outBuff.data(), outBuff.size(), inBuff.data(), readSize, nullptr);
    aCompressed.write(outBuff.data(), outSize);

    aSource.read(inBuff.data(), blockSize);
    readSize = aSource.gcount();
  }
  headerSize = LZ4F_compressEnd(context, outBuff.data(), outBuff.size(), nullptr);
  aCompressed.write(outBuff.data(), headerSize);

  LZ4F_errorCode_t errorCode = LZ4F_freeCompressionContext(context);
  if (LZ4F_isError(errorCode))
    throw std::runtime_error("LZ4 decompression error: " + std::string(LZ4F_getErrorName(errorCode)));
}

void ZCompressStream(std::istream& aSource, std::ostream& aCompressed) {
  zstr::ostream outputStream(aCompressed);
  std::copy(
    std::istreambuf_iterator<char>(aSource),
    std::istreambuf_iterator<char>(),
    std::ostreambuf_iterator<char>(aCompressed)
  );
}


auto Magic2Int(const TMagic4& aMagic) -> Cardinal
{
  return *(PCardinal)(aMagic.data());
}

auto Int2Magic(Cardinal aInt) -> TMagic4
{
  TMagic4 Result;
  *(PCardinal)Result.data() = aInt;
  return Result;
}

auto String2Magic(const std::string& aStr) -> TMagic4
{
  TMagic4 Result = {'\x0', '\x0', '\x0', '\x0'};
  if (aStr.length() > 0) Result[0] = aStr[1];
  if (aStr.length() > 1) Result[1] = aStr[2];
  if (aStr.length() > 2) Result[2] = aStr[3];
  if (aStr.length() > 3) Result[3] = aStr[4];
  return Result;
}

auto LowerByte(char ch) -> Byte
{
  if('A' <= ch && ch <= 'Z')
    return Byte(int(ch) + int('a')-int('A'));
  else
    return Byte(ch);
}

auto Str2MagicInt(const std::string& s) -> Cardinal
{
  Cardinal Result = 0;
  for (size_t i = 0; i < s.length(); ++i) {
    *(PByte)((PByte)(&Result) + i) = LowerByte(s[i]);
    if (i == 3) break;
  }
  return Result;
}

auto SplitDirName(const std::string& aFileName, std::string& Dir, std::string& Name) -> size_t
{
  size_t Result = aFileName.find_last_of('\\');
  if (Result == std::string::npos)
    Result = aFileName.find_last_of('/');

  if (Result == std::string::npos) {
      Dir = "";
      Name = aFileName;
      return 0;
  }

  Dir = aFileName.substr(0, Result);
  Name = aFileName.substr(Result+1);
  return Result;
}

auto SplitNameExt(const std::string& aFileName, std::string& Name, std::string& Ext) -> size_t
{
  size_t Result = aFileName.find_last_of('.');
  if (Result != std::string::npos) {
    Name = aFileName.substr(0, Result);
    Ext = aFileName.substr(Result);
  }
  else {
    Name = aFileName;
    Ext = "";
  }
  return Result;
}

auto CreateHashTES3(const std::string& aFileName) -> UInt64
{
  UInt64 Result;
  std::string s;
  Cardinal sum, off, temp, n, i, l;

  s = aFileName;
  l = s.length() >> 1;

  sum = 0, off = 0;
  for (i = 0; i < l; ++i) {
    temp = Cardinal(LowerByte(s[i])) << (off & 0x1F);
    sum = sum ^ temp;
    off = off + 8;
  }
  Result = UInt64(sum) << 32;

  sum = 0, off = 0;
  for (i = l; i< s.size(); ++i) {
    temp = (Cardinal(LowerByte(s[i]))) << (off & 0x1F);
    sum = sum ^ temp;
    n = temp & 0x1F;
    sum = (sum << (32 - n)) | (sum >> n);
    off += 8;
  }
  return (Result | sum);
}

UInt64 CreateHashTES4(std::string stem, std::string extension)
{
  size_t len = stem.length();
  if (len == 0)
    return 0;
  std::replace(stem.begin(), stem.end(), '/', '\\');
  Misc::StringUtils::lowerCaseInPlace(stem);
  uint64_t result = stem[len - 1] | (len >= 3 ? (stem[len - 2] << 8) : 0) | (len << 16) | (stem[0] << 24);
  if (len >= 4)
  {
    uint32_t hash = 0;
    for (size_t i = 1; i <= len - 3; ++i)
      hash = hash * 0x1003f + stem[i];
    result += static_cast<uint64_t>(hash) << 32;
  }
  if (extension.empty())
    return result;
  Misc::StringUtils::lowerCaseInPlace(extension);
  if (extension == ".kf")       result |= 0x80;
  else if (extension == ".nif") result |= 0x8000;
  else if (extension == ".dds") result |= 0x8080;
  else if (extension == ".wav") result |= 0x80000000;
  uint32_t hash = 0;
  for (const char& c : extension)
    hash = hash * 0x1003f + c;
  result += static_cast<uint64_t>(hash) << 32;
  return result;
}

auto CreateHashTES4(const std::string& aFileName) -> UInt64
{
  std::string fname, fext;

  SplitNameExt(aFileName, fname, fext);
  return CreateHashTES4(fname, fext);
}

Cardinal CreateHashFO4(const std::string& aFileName)
{
  size_t i;
  std::string s;
  char c;

  Cardinal Result = 0;
  s = aFileName;
  for (i = 0; i < s.size(); ++i) {
    c = s[i];
    if (Byte(c) > 127) continue;
    if (c == '/') c = '\\';
    Result = (Result >> 8) ^ crc32table[(Result ^ LowerByte(c)) & 0xFF];
  }
  return Result;
}

auto TwbBSFileTES4::Compressed(const TwbBSArchive& bsa) const -> bool
{
  return (bsa.GetArchiveFlags() & ARCHIVE_COMPRESS) || (Size & FILE_SIZE_COMPRESS);
}

auto TwbBSFileTES4::RawSize() const -> Cardinal
{
  return Size & ~FILE_SIZE_COMPRESS;
}


TwbBSArchive::TwbBSArchive()
{
  fType = baNone;
  fMaxChunkCount = 4;
  fSingleMipChunkX = 512;
  fSingleMipChunkY = 512;
}

TwbBSArchive::~TwbBSArchive()
{
  if (fStates & (stReading|stWriting))
    Close();
}

auto TwbBSArchive::GetArchiveFormatName() const -> std::string
{
  return TBSArchiveFormatName[fType];
}

auto TwbBSArchive::GetFileCount() const -> Cardinal
{
  switch(fType) {
    case baTES3:
      return fHeaderTES3.FileCount;
    case baTES4: case baFO3: case baSSE:
      return fHeaderTES4.FileCount;
    case baFO4: case baFO4dds:
      // because we use fHeaderFO4.FileCount as the current index in new archives
      return fFilesFO4.size();
    default:
      return 0;
  }
}

void TwbBSArchive::SetArchiveFlags(Cardinal aFlags)
{
  switch (fType) {
  case baTES4: case baFO3: case baSSE:
      break;
  default:
      throw std::runtime_error("Archive flags are not supported for this archive type");
  }
  fHeaderTES4.Flags = aFlags;

  // force compression flag if needed
  if (fCompress)
    fHeaderTES4.Flags |= ARCHIVE_COMPRESS;
}

bool TwbBSArchive::FindFileRecordTES3(const std::string& aFileName, Integer& aFileIdx) const
{
  UInt64 h = CreateHashTES3(aFileName);

  for(size_t i = 0; i < fFilesTES3.size(); ++i) {
    if (fFilesTES3[i].Hash == h) {
      aFileIdx = i;
      return true;
    }
  }
  return false;
}

bool TwbBSArchive::FindFileRecordTES4(const std::string& aFileName, Integer& aFolderIdx, Integer& aFileIdx) const
{
  std::string fdir, fname, name, ext;
  UInt64 h;
  size_t i, j;

  if (SplitDirName(aFileName, fdir, fname) == std::string::npos)
    return false;

  h = CreateHashTES4(fdir, "");
  for (i = 0; i < fFoldersTES4.size(); ++i) {
    const auto& d = fFoldersTES4[i];
    if (h != d.Hash) {
      // since table is sorted by hash, we can abort when our hash is lesser
      if (h < d.Hash)
        return false;
      else
        continue;
    }
    SplitNameExt(fname, name, ext);
    h = CreateHashTES4(name, ext);
    for (j = 0; j < d.Files.size(); ++j) {
      const auto& f = d.Files[j];
      if (h != f.Hash) {
        if (h < f.Hash)
          return false;
        else
          continue;
      }
      aFolderIdx = i;
      aFileIdx = j;
      return true;
    }
  }
  return false;
}

bool TwbBSArchive::FindFileRecordFO4(const std::string& aFileName, Integer& aFileIdx) const
{
  std::string fdir, fname, name, ext;
  Cardinal hdir, hfile;
  TMagic4 hext;
  size_t i;

  if (SplitDirName(aFileName, fdir, fname) == std::string::npos)
    return false;

  SplitNameExt(fname, name, ext);
  hdir = CreateHashFO4(fdir);
  hfile = CreateHashFO4(name);
  if (ext.front() == '.')
    ext.erase(ext.begin());
  hext = String2Magic(Misc::StringUtils::lowerCase(ext));

  for (i = 0; i < fFilesFO4.size(); ++i) {
    if ((fFilesFO4[i].DirHash == hdir) && (fFilesFO4[i].NameHash == hfile) && (fFilesFO4[i].Ext == hext)) {
      aFileIdx = i;
      return true;
    }
  }
  return false;
}

Pointer TwbBSArchive::FindFileRecord(const std::string& aFileName)
{
  Integer i, j;
  Pointer Result = nullptr;

  switch(fType) {
    case baTES3:
      if (FindFileRecordTES3(aFileName, i)) Result = &fFilesTES3[i];
      break;
    case baTES4: case baFO3: case baSSE:
      if (FindFileRecordTES4(aFileName, i, j)) Result = &fFoldersTES4[i].Files[j];
      break;
    case baFO4: case baFO4dds:
      if (FindFileRecordFO4(aFileName, i)) Result = &fFilesFO4[i];
      break;
    default:
      break;
  }
  return Result;
}

Integer TwbBSArchive::GetDDSMipChunkNum(TDDSInfo& aDDSInfo) const //[TODO] check usage start 1
{
  Integer w, h;

  w = aDDSInfo.Width;
  h = aDDSInfo.Height;
  Integer Result = 1;
  while ((Result < aDDSInfo.MipMaps) &&
        (Result < fMaxChunkCount) &&
        (w >= fSingleMipChunkX) &&
        (h >= fSingleMipChunkY))
  {
    ++Result;
    w = w / 2;
    h = h / 2;
  }
  return Result;
}

TPackedDataHash TwbBSArchive::CalcDataHash(const char *aData, Cardinal aLen) const
{
  TPackedDataHash result;
  MD5((const unsigned char*)aData, aLen, &result[0]);
  return result;
}

bool TwbBSArchive::FindPackedData(Cardinal aSize, const TPackedDataHash& aHash, Pointer aFileRecord)
{
  if (!fShareData)
    return false;

  for (auto& p : fPackedData) {
    if ((aSize == p.Size) && (aHash == p.Hash)) {
      switch(fType) {
        case baTES3: {
          PwbBSFileTES3(aFileRecord)->Size = PwbBSFileTES3(p.FileRecord)->Size;
          PwbBSFileTES3(aFileRecord)->Offset = PwbBSFileTES3(p.FileRecord)->Offset;
          break;
        }
        case baTES4: case baFO3: case baSSE: {
          PwbBSFileTES4(aFileRecord)->Size = PwbBSFileTES4(p.FileRecord)->Size;
          PwbBSFileTES4(aFileRecord)->Offset = PwbBSFileTES4(p.FileRecord)->Offset;
          break;
        }
        case baFO4: {
          PwbBSFileFO4(aFileRecord)->Size = PwbBSFileFO4(p.FileRecord)->Size;
          PwbBSFileFO4(aFileRecord)->PackedSize = PwbBSFileFO4(p.FileRecord)->PackedSize;
          PwbBSFileFO4(aFileRecord)->Offset = PwbBSFileFO4(p.FileRecord)->Offset;
          break;
        }
        case baFO4dds: {
          PwbBSTexChunkRec(aFileRecord)->Size = PwbBSTexChunkRec(p.FileRecord)->Size;
          PwbBSTexChunkRec(aFileRecord)->PackedSize = PwbBSTexChunkRec(p.FileRecord)->PackedSize;
          PwbBSTexChunkRec(aFileRecord)->Offset = PwbBSTexChunkRec(p.FileRecord)->Offset;
          break;
        }
        default:
          break;
      }
      return true;
    }
  }
  return false;
}

void TwbBSArchive::AddPackedData(Cardinal aSize, const TPackedDataHash& aHash, Pointer aFileRecord)
{
  if (!fShareData)
    return;

  if (fPackedData.empty())
    fPackedData.reserve(2048);
  TPackedDataInfo info;
  info.Size = aSize;
  info.Hash = aHash;
  info.FileRecord = aFileRecord;
  fPackedData.push_back(info);
}

template<typename T>
T read(std::istream& in)
{
  T result;
  in.read(reinterpret_cast<char*>(&result), sizeof(T));
  return result;
}

std::string ReadStringTerm(std::istream& in)
{
  char c;
  std::string result;
  do
  {
    in.read(&c, 1);
    result.push_back(c);
  } while(c != '\0');
  result.pop_back();
  return result;
}

std::string ReadStringLen(std::istream& in, bool Term = true)
{
    std::string result;
    Byte len = read<Byte>(in);
    result.resize(len);
    in.read(result.data(), result.size());
    if (Term && !result.empty())
        result.pop_back();
    return result;
}

std::string ReadStringLen16(std::istream& in)
{
    std::string result;
    Word len = read<Word>(in);
    result.resize(len);
    in.read(result.data(), result.size());
    return result;
}

std::string StringReplaceAll(std::string where, char what, char replacement)
{
  std::replace(where.begin(), where.end(), what, replacement);
  return where;
}

void TwbBSArchive::LoadFromFile(const std::string& aFileName)
{
  if (!(fStates & (stReading|stWriting)))
    Close();

  fStream = std::fstream(aFileName, std::ios::binary | std::ios::in);

  // magic
  fMagic = Int2Magic(read<Cardinal>(fStream));
  if (fMagic == MAGIC_TES3)      fType = baTES3;
  else if (fMagic == MAGIC_BSA)  fType = baTES4;
  else if (fMagic == MAGIC_BTDX) fType = baFO4;
  else throw std::runtime_error("Unknown archive format");

  // archive version except Morrowind
  if (fType != baTES3) {
    fVersion = read<Cardinal>(fStream);
    switch(fVersion) {
      case HEADER_VERSION_TES4: fType = baTES4; break;
      case HEADER_VERSION_FO3 : fType = baFO3; break;
      case HEADER_VERSION_SSE : fType = baSSE; break;
      case HEADER_VERSION_FO4 : fType = baFO4; break;
    default:
      std::stringstream stream;
      stream << std::hex << fVersion;
      throw std::runtime_error("Unknown archive version 0x" + stream.str());
    }
  }

  switch(fType) {
    //--------------------------------------------------
    // Morrowind
    case baTES3: {
      // read header
      fHeaderTES3 = read<TwbBSHeaderTES3>(fStream);
      fFilesTES3.resize(fHeaderTES3.FileCount);
      for(auto& f : fFilesTES3) {
        f.Size = read<Cardinal>(fStream);
        f.Offset = read<Cardinal>(fStream);
      }
      // skip name offsets
      fStream.seekg(4 * fHeaderTES3.FileCount, std::ios_base::cur);
      // read names
      for(auto& f : fFilesTES3)
        f.Name = ReadStringTerm(fStream);  //[TODO] performance issue: read all in one block and then split
      // read hashes
      for(auto& f : fFilesTES3)
        f.Hash = read<UInt64>(fStream);
      // remember binary data offset since stored files offsets are relative
      fDataOffset = fStream.tellg();
      break;
    }

    //--------------------------------------------------
    // Fallout 4
    case baFO4: {
      // read header
      fHeaderFO4 = read<TwbBSHeaderFO4>(fStream);

      // read GNRL files
      if (to_array(fHeaderFO4.Magic) == MAGIC_GNRL) {
        fFilesFO4.resize(fHeaderFO4.FileCount);
        for (auto& f : fFilesFO4) {
          f.NameHash = read<Cardinal>(fStream);
          f.Ext = Int2Magic(read<Cardinal>(fStream));
          f.DirHash = read<Cardinal>(fStream);
          f.Unknown = read<Cardinal>(fStream);
          f.Offset = read<UInt64>(fStream);
          f.PackedSize = read<Cardinal>(fStream);
          f.Size = read<Cardinal>(fStream);
          read<Cardinal>(fStream); // BAADF00D
        }
      }
      // read DX10 textures
      else if (to_array(fHeaderFO4.Magic) == MAGIC_DX10) {
        fType = baFO4dds;
        fFilesFO4.resize(fHeaderFO4.FileCount);
        for (auto& f : fFilesFO4) {
          f.NameHash = read<Cardinal>(fStream);
          f.Ext = Int2Magic(read<Cardinal>(fStream));
          f.DirHash = read<Cardinal>(fStream);
          f.UnknownTex = read<Byte>(fStream);
          f.TexChunks.resize(read<Byte>(fStream));
          read<Word>(fStream); // skip chunkHeaderSize, always 24
          //f.ChunkHeaderSize := fStream.ReadWord;
          f.Height = read<Word>(fStream);
          f.Width = read<Word>(fStream);
          f.NumMips = read<Byte>(fStream);
          f.DXGIFormat = read<Byte>(fStream);
          f.CubeMaps = read<Word>(fStream);
          for (auto& c : f.TexChunks) {
            c.Offset = read<UInt64>(fStream);
            c.PackedSize = read<Cardinal>(fStream);
            c.Size = read<Cardinal>(fStream);
            c.StartMip = read<Word>(fStream);
            c.EndMip = read<Word>(fStream);
            read<Cardinal>(fStream); // skip BAADF00D
          }
        }
      }
      else
        throw std::runtime_error("Unknown BA2 archive type");

      // read file names
      fStream.seekg(fHeaderFO4.FileTableOffset);
      for (auto& f : fFilesFO4)
        f.Name = StringReplaceAll(ReadStringLen16(fStream), '\\', '/');
      break;
    }

    //--------------------------------------------------
    // Oblivion, Fallout 3, New Vegas, Skyrim, Skyrim SE
    case baTES4: case baFO3: case baSSE: {
      // read header
      fHeaderTES4 = read<TwbBSHeaderTES4>(fStream);
      fStream.seekg(fHeaderTES4.FoldersOffset);

      // read folder records
      fFoldersTES4.resize(fHeaderTES4.FolderCount);
      for (auto& d : fFoldersTES4) {
        d.Hash = read<UInt64>(fStream);
        d.FileCount = read<Cardinal>(fStream);
        if (fType == baSSE) {
          d.Unk32 = read<Cardinal>(fStream);
          d.Offset = read<UInt64>(fStream);
        } else
          d.Offset = read<Cardinal>(fStream);
      }

      // read folder names and file records
      for (auto& d : fFoldersTES4) {
        d.Name = ReadStringLen(fStream);
        d.Files.resize(d.FileCount);
        for (auto& f : d.Files) {
          f.Hash = read<UInt64>(fStream);
          f.Size = read<Cardinal>(fStream);
          f.Offset = read<Cardinal>(fStream);
        }
      }

      // read file names
      for (auto& d : fFoldersTES4)
        for (auto& f : d.Files)
          f.Name = ReadStringTerm(fStream);

      for (auto& d : fFoldersTES4)
          for (auto& f : d.Files)
              f.FullName = d.Name + "\\" + f.Name;
      break;
    }
    default:
      break;

  }

  fFileName = aFileName;
  fStates |= stReading;
}

struct THashPair { UInt64 DirHash, FileHash; };
using PHashPair = THashPair*;

bool HashPairSort(const THashPair& left, const THashPair& right)
{
  if (left.DirHash < right.DirHash)
    return true;
  else if (left.DirHash > right.DirHash)
    return false;
  else if (left.FileHash < right.FileHash)
    return true;
  else if (left.FileHash > right.FileHash)
    return false;
  return true;
}

/*function AlphabeticalSort(List: TStringList; Index1, Index2: Integer): Integer;
{
  Result := CompareStr(List[Index1], List[Index2]);
}*/
void TwbBSArchive::CreateArchive(const std::string& aFileName, TBSArchiveType aType,
  std::vector<std::string>& aFilesList)
{
  std::vector<THashPair> HashPairs;
  PHashPair h;
  UInt64 hdir;
  std::string s, fdir, fname, fext;
  Integer len;
  TBytes Buffer;
  TDDSInfo ddsinfo;

  if (stReading & fStates)
    Close();

  if (stWriting & fStates)
    throw std::runtime_error("Archive is already being created");

  switch(aType) {
    case baTES3: {
      fMagic = MAGIC_TES3;
      break;
    }
    case baTES4: {
      fVersion = HEADER_VERSION_TES4;
      fMagic = MAGIC_BSA;
      fHeaderTES4.Flags = ARCHIVE_PATHNAMES | ARCHIVE_FILENAMES | ARCHIVE_EMBEDNAME | ARCHIVE_XMEM | ARCHIVE_UNKNOWN10;
      fHeaderTES4.FileFlags = 0;
      fHeaderTES4.FoldersOffset = fMagic.size() + sizeof(fVersion) + sizeof(fHeaderTES4);
      break;
    }
    case baFO3: {
      fVersion = HEADER_VERSION_FO3;
      fMagic = MAGIC_BSA;
      fHeaderTES4.Flags = ARCHIVE_PATHNAMES | ARCHIVE_FILENAMES;
      fHeaderTES4.FileFlags = 0;
      fHeaderTES4.FoldersOffset = fMagic.size() + sizeof(fVersion) + sizeof(fHeaderTES4);
      break;
    }
    case baSSE: {
      fVersion = HEADER_VERSION_SSE;
      fMagic = MAGIC_BSA;
      fHeaderTES4.Flags = ARCHIVE_PATHNAMES | ARCHIVE_FILENAMES;
      fHeaderTES4.FileFlags = 0;
      fHeaderTES4.FoldersOffset = fMagic.size() + sizeof(fVersion) + sizeof(fHeaderTES4);
      break;
    }
    case baFO4: {
      fMagic = MAGIC_BTDX;
      std::copy(MAGIC_GNRL.data(), MAGIC_GNRL.data() + 4, fHeaderFO4.Magic);
      fVersion = HEADER_VERSION_FO4;
      break;
    }
    case baFO4dds: {
      fMagic = MAGIC_BTDX;
      std::copy(MAGIC_DX10.data(), MAGIC_DX10.data() + 4, fHeaderFO4.Magic);
      fVersion = HEADER_VERSION_FO4;
      break;
    }
  default:
    throw std::runtime_error("Unsupported archive type");
  }

  fType = aType;

  switch(fType) {
    case baTES3: {
      if (aFilesList.empty())
        throw std::runtime_error("Archive requires predefined files list");

      // sort files by hashes
      HashPairs.resize(aFilesList.size());
      for(size_t i = 0; i < aFilesList.size(); ++i) {
        h = &HashPairs[i];
        h->FileHash = CreateHashTES3(aFilesList[i]);
      }
      //aFilesList.CustomSort(HashPairSort);
      std::vector<int> indices(aFilesList.size());
      std::iota(indices.begin(), indices.end(), 0);
      std::sort(indices.begin(), indices.end(), [&](int left, int right) {
        return HashPairSort(HashPairs[left], HashPairs[right]);
      });

      // create file records and calculate total names length
      fFilesTES3.resize(aFilesList.size());
      len = 0;
      for (size_t i = 0; i < aFilesList.size(); ++i) {
        fFilesTES3[i].Hash = HashPairs[indices[i]].FileHash;
        fFilesTES3[i].Name = Misc::StringUtils::lowerCase(aFilesList[indices[i]]);
        len += fFilesTES3[i].Name.size() + 1; // include terminator
      }

      // offset to hash table
      fDataOffset = fMagic.size() + sizeof(fHeaderTES3) +
        8 * fFilesTES3.size() + // File sizes/offsets
        4 * fFilesTES3.size() + // Archive directory/name offsets
        len; // Filename records

      // stored as minus 12 (for header size)
      fHeaderTES3.HashOffset = fDataOffset - 12;
      fHeaderTES3.FileCount = aFilesList.size();

      // offset to files data
      fDataOffset = fDataOffset + 8 * fFilesTES3.size(); // Hash table

      // files are stored alphabetically in the data section in vanilla archives
      // not really needed but whatever
      std::sort(aFilesList.begin(), aFilesList.end());
      break;
    }

    case baTES4: case baFO3: case baSSE: {
      if (aFilesList.empty())
        throw std::runtime_error("Archive requires predefined files list");

      fHeaderTES4.FolderNamesLength = 0;
      fHeaderTES4.FileNamesLength = 0;

      // dirs and files must be sorted by their hashes
      HashPairs.resize(aFilesList.size());
      for (size_t i = 0; i < aFilesList.size(); ++i) {
        h = &HashPairs[i];
        if (SplitDirName(aFilesList[i], fdir, fname) == std::string::npos)
          throw std::runtime_error("File is missing the folder part: " + aFilesList[i]);

        // calculate hashes
        h->DirHash = CreateHashTES4(fdir, "");
        SplitNameExt(fname, s, fext);
        h->FileHash = CreateHashTES4(s, fext);
        //aFilesList.Objects[i] := Pointer(h);

        // determine file flags
        fext = Misc::StringUtils::lowerCase(fext);
        if (fext == ".nif") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_NIF;
        else if (fext == ".dds") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_DDS;
        else if (fext == ".xml") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_XML | FILE_MISC;
        else if (fext == ".wav") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_WAV;
        else if (fext == ".fuz") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_WAV;
        else if (fext == ".mp3") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_MP3;
        else if (fext == ".ogg") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_MP3;
        else if (fext == ".txt") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_TXT;
        else if (fext == ".htm") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_TXT;
        else if (fext == ".bat") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_TXT;
        else if (fext == ".scc") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_TXT;
        else if (fext == ".spt") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_SPT;
        else if (fext == ".fnt") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_FNT;
        else if (fext == ".tex") fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_FNT;
        else fHeaderTES4.FileFlags = fHeaderTES4.FileFlags | FILE_MISC;
      }
      // sort by hashes
      //aFilesList.CustomSort(HashPairSort);
      std::vector<int> indices(aFilesList.size());
      std::iota(indices.begin(), indices.end(), 0);
      std::sort(indices.begin(), indices.end(), [&](int left, int right) {
          return HashPairSort(HashPairs[left], HashPairs[right]);
      });

      // create folder and file records
      fHeaderTES4.FileCount = 0;
      hdir = 0;


      for (size_t i = 0; i < aFilesList.size(); ++i) {
        SplitDirName(aFilesList[indices[i]], fdir, fname);
        h = &HashPairs[indices[i]];
        // new folder
        if (h->DirHash != hdir) {
          hdir = h->DirHash;
          TwbBSFolderTES4 dir = {};
          dir.Hash = h->DirHash;
          dir.Name = Misc::StringUtils::lowerCase(fdir);
          fFoldersTES4.push_back(dir);
          // calc folder names length
          fHeaderTES4.FolderNamesLength += fdir.size() + 1; // + terminator only, length prefix is not counted
        }
        TwbBSFileTES4 file = {};
        file.Hash = h->FileHash;
        file.Name = Misc::StringUtils::lowerCase(fname);
        file.FullName = fFoldersTES4.back().Name + "\\" + file.Name;
        fFoldersTES4.back().Files.push_back(file);
        ++fFoldersTES4.back().FileCount;//[TODO] do not use this member use the size() method of the Files
        ++fHeaderTES4.FileCount;
        // calculate file names length
        fHeaderTES4.FileNamesLength += fname.size() + 1; // + terminator
      }
      fHeaderTES4.FolderCount = fFoldersTES4.size();

      // calculate folders offsets
      // at the end fDataOffset will hold the total size of header, folder and file records
      // in other words the start of files data
      fDataOffset = fMagic.size() + sizeof(fVersion) + sizeof(fHeaderTES4) + 16 * fFoldersTES4.size();
      // SSE folder record is 8 bytes larger
      if (fType == baSSE)
        fDataOffset += 8 * fFoldersTES4.size();
      // Offsets are stored including this value
      fDataOffset += fHeaderTES4.FileNamesLength;
      for (auto& d : fFoldersTES4) {
        d.Offset = fDataOffset;
        // add folder name length
        fDataOffset += d.Name.size() + 2; // + length prefix + terminator
        // add file records length
        fDataOffset += 16 * d.Files.size();
      }

      // final flags detection

      // misc file flag is not in Skyrim
      if (fType == baSSE)
        fHeaderTES4.FileFlags = fHeaderTES4.FileFlags & ~FILE_MISC;

      // embedded names in texture only archives
      if (fHeaderTES4.FileFlags == FILE_DDS)
        fHeaderTES4.Flags = fHeaderTES4.Flags | ARCHIVE_EMBEDNAME;

      // startupstr flag in archives with meshes
      if (fHeaderTES4.FileFlags & FILE_NIF)
        fHeaderTES4.Flags = fHeaderTES4.Flags | ARCHIVE_STARTUPSTR;

      // retainname flag in archives with sounds
      if (fHeaderTES4.FileFlags & FILE_WAV)
        fHeaderTES4.Flags = fHeaderTES4.Flags | ARCHIVE_RETAINNAME;

      // txt, xml and fnt file flags are exclusive for Oblivion
      if (fType == baTES4)
        fHeaderTES4.FileFlags = fHeaderTES4.FileFlags & ~(FILE_XML | FILE_TXT | FILE_FNT);

      // set compression flag if needed
      if (fCompress)
        fHeaderTES4.Flags = fHeaderTES4.Flags | ARCHIVE_COMPRESS;
      break;
    }

    case baFO4: case baFO4dds: {
      if (aFilesList.empty())
        throw std::runtime_error("Archive requires predefined files list");

      fFilesFO4.resize(aFilesList.size());
      fDataOffset = fMagic.size() + sizeof(fVersion) + sizeof(fHeaderFO4);

      // file records have fixed length in general archive
      if (fType == baFO4)
        fDataOffset = fDataOffset + 36 * fFilesFO4.size();

      // variable file record length depending on DDS chunks number
      else if (fType == baFO4dds) {
        if (!fDDSInfoProc)
          throw std::runtime_error("DDS archive requires DDS file information callback");

        for (const auto& f : aFilesList) {
          fDDSInfoProc(*this, f, ddsinfo);
          fDataOffset = fDataOffset + 24 /*size of file record*/ + 24 /*size of each texchunk*/ * GetDDSMipChunkNum(ddsinfo);
        }
      }
      break;
    }
    default:
      break;
  }

  if (!std::filesystem::exists(aFileName)) {
      std::filesystem::remove(aFileName);
      std::ofstream().open(aFileName);
  }
  fStream = std::fstream(aFileName, std::ios::binary | std::ios::in | std::ios::out);
  fFileName = aFileName;
  fStates |= stWriting;

  // reserve space for the header
  Buffer.resize(fDataOffset);
  fStream.write(Buffer.data(), Buffer.size());
}

template<typename T>
void write(std::ostream& out, T const& value) {
  out.write(reinterpret_cast<char const*>(&value), sizeof(T));
}

template<>
void write(std::ostream& out, std::string const& value) {
  out.write(value.data(), value.size());
  char end='\0';
  out.write(&end, 1);
}

void WriteStringTerm(std::ostream& out, const std::string& value)
{
  out.write(value.c_str(), value.size());
  char end = '\0';
  out.write(&end, 1);
}

void WriteStringLen16(std::ostream& out, const std::string& value)
{
  uint16_t Len = value.size();
  out.write(reinterpret_cast<char const*>(&Len), sizeof(Len));
  out.write(value.c_str(), value.size());
}

void WriteStringLen(std::ostream& out, const std::string& value, bool term=true)
{
  uint8_t Len = value.size() + (term? 1 : 0);
  out.write(reinterpret_cast<char const*>(&Len), sizeof(Len));
  out.write(value.c_str(), value.size());
  if (term) {
    char end = '\0';
    out.write(&end, 1);
  }
}

void TwbBSArchive::Save()
{
  if (!(fStates & stWriting))
    throw std::runtime_error("Archive is not in writing mode");

  switch(fType) {
    case baTES3: {
      for(auto const& f : fFilesTES3)
        if (f.Offset == 0)
          throw std::runtime_error("Archived file has no data: " + f.Name);

      // write header
      fStream.seekp(0);
      // magic, header record
      fStream.write(fMagic.data(), fMagic.size());
      write(fStream, fHeaderTES3);
      // file sizes/offsets
      for (auto const& f : fFilesTES3) {
        write<Cardinal>(fStream, f.Size);
        write<Cardinal>(fStream, f.Offset - fDataOffset); // offsets are relative
      }
      // Archive directory/name offsets
      size_t j = 0;
      for (auto const& f : fFilesTES3) {
        write<Cardinal>(fStream, j);
        j += f.Name.size() + 1; // including terminator
      }
      // Filename records
      for (auto const& f : fFilesTES3)
        WriteStringTerm(fStream, f.Name);
      // Hash table
      for (auto const& f : fFilesTES3) {
        write<Cardinal>(fStream, f.Hash >> 32);
        write<Cardinal>(fStream, f.Hash & 0xFFFFFFFF);
      }
      break;
    }

    case baTES4: case baFO3: case baSSE: {
      // check that all files from files table have saved data
      for (const auto& d : fFoldersTES4)
        for (const auto& f : d.Files)
          if (f.Offset == 0)
            throw std::runtime_error("Archived file has no data: " + d.Name + "\\" + f.Name);

      // write header
      fStream.seekp(0);
      // magic, version, header record
      fStream.write(fMagic.data(), fMagic.size());
      write(fStream, fVersion);
      write(fStream, fHeaderTES4);
      // folder records
      for (const auto& d : fFoldersTES4) {
        write<UInt64>(fStream, d.Hash);
        write<Cardinal>(fStream, d.FileCount);
        if (fType == baSSE) {
          write<Cardinal>(fStream, d.Unk32);
          write<Int64>(fStream, d.Offset);
        } else
          write<Cardinal>(fStream, d.Offset);
      }
      // file records
      for (const auto& d : fFoldersTES4) {
        WriteStringLen(fStream, d.Name);
        for (const auto& f : d.Files) {
          write<UInt64>(fStream, f.Hash);
          write<Cardinal>(fStream, f.Size);
          write<Cardinal>(fStream, f.Offset);
        }
      }
      // file names
      for (const auto& d : fFoldersTES4)
        for (const auto& f : d.Files)
          WriteStringTerm(fStream, f.Name);
      break;
    }

    case baFO4: {
      for (const auto& f : fFilesFO4)
        if (f.Offset == 0)
          throw std::runtime_error("Archived file has no data: " + f.Name);

      // file names table
      fHeaderFO4.FileTableOffset = fStream.tellp();
      for (const auto& f : fFilesFO4)
        WriteStringLen16(fStream, f.Name);
      // write header
      fStream.seekp(0);
      // magic, version, header record
      fStream.write(fMagic.data(), fMagic.size());
      write(fStream, fVersion);
      write(fStream, fHeaderFO4);
      // file records
      for (const auto& f : fFilesFO4) {
        write<Cardinal>(fStream, f.NameHash);
        fStream.write(f.Ext.data(), f.Ext.size());
        write<Cardinal>(fStream, f.DirHash);
        write<Cardinal>(fStream, f.Unknown);
        write<Int64>(fStream, f.Offset);
        write<Cardinal>(fStream, f.PackedSize);
        write<Cardinal>(fStream, f.Size);
        write<Cardinal>(fStream, iFileFO4Tail);
      }
      break;
    }

    case baFO4dds: {
      for (auto const& f : fFilesFO4)
        if (f.TexChunks.empty())
          throw std::runtime_error("Archived file has no data: " + f.Name);

      // file names table
      fHeaderFO4.FileTableOffset = fStream.tellp();
      for (auto const& f : fFilesFO4)
        WriteStringLen16(fStream, f.Name);
      // write header
      fStream.seekg(0);
      // magic, version, header record
      fStream.write(fMagic.data(), fMagic.size());
      write(fStream, fVersion);
      write(fStream, fHeaderFO4);
      // file records
      for (auto const& f : fFilesFO4) {
        write<Cardinal>(fStream, f.NameHash);
        fStream.write(f.Ext.data(), f.Ext.size());
        write<Cardinal>(fStream, f.DirHash);
        write<Byte>(fStream, f.UnknownTex);
        write<Byte>(fStream, f.TexChunks.size());
        write<Word>(fStream, 24); // fixed chunk header size
        write<Word>(fStream, f.Height);
        write<Word>(fStream, f.Width);
        write<Byte>(fStream, f.NumMips);
        write<Byte>(fStream, f.DXGIFormat);
        write<Word>(fStream, f.CubeMaps);
        for (auto const& c : f.TexChunks) {
          write<UInt64>(fStream, c.Offset);
          write<Cardinal>(fStream, c.PackedSize);
          write<Cardinal>(fStream, c.Size);
          write<Word>(fStream, c.StartMip);
          write<Word>(fStream, c.EndMip);
          write<Cardinal>(fStream, iFileFO4Tail);
        }
      }
      break;
    }
    default:
      break;
  }

  fStream.close();
  fStates &= ~stWriting;
  Close();
}

void TwbBSArchive::AddFile(const std::string& aRootDir, const std::string& aFileName)
{
  TBytes Buffer;
  if (!(stWriting & fStates))
    throw std::runtime_error("Archive is not in writing mode");

  Integer i = aRootDir.size();
  if ((i > 0) && (aRootDir.back() != '\\'))
    ++i;

  std::string fname = aFileName.substr(i);

  Buffer.resize(std::filesystem::file_size(aFileName));
  std::ifstream stream(aFileName, std::ios::binary);
  stream.read(Buffer.data(), Buffer.size());

  AddFile(fname, Buffer);
}

void TwbBSArchive::AddFile(const std::string& aFileName, const TBytes& aData)
{
  Integer i, j, Off, MipSize, BitsPerPixel;
  std::string fdir, fname, name, ext;
  bool wasCompressed;
  TPackedDataHash DataHash;
  PDDSHeader DDSHeader;
  PDDSHeaderDX10 DDSHeaderDX10;
  TDDSInfo DDSInfo;

  if (!(stWriting & fStates))
    throw std::runtime_error("Archive is not in writing mode");

  // FO4 dds mipmaps have their own partial hash calculation down below
  if (fShareData && (fType != baFO4dds))
    DataHash = CalcDataHash(aData.data(), aData.size());

    switch(fType) {
      case baTES3: {
        if (!FindFileRecordTES3(aFileName, i))
          throw std::runtime_error("File not found in files table: " + aFileName);

        if (FindPackedData(aData.size(), DataHash, &fFilesTES3[i]))
          return;

        fFilesTES3[i].Size = aData.size();
        fFilesTES3[i].Offset = fStream.tellp();
        fStream.write(aData.data(), aData.size());
        AddPackedData(aData.size(), DataHash, &fFilesTES3[i]);
        break;
      }

      case baTES4: case baFO3: case baSSE: {
        if (!FindFileRecordTES4(aFileName, i, j))
          throw std::runtime_error("File not found in files table: " + aFileName);

        if (FindPackedData(aData.size(), DataHash, &fFoldersTES4[i].Files[j]))
          return;

        std::unique_ptr<std::stringstream> MemoryStream;
        fFoldersTES4[i].Files[j].Offset = fStream.tellp();

        if ((fType == baFO3 || fType == baSSE) && (fHeaderTES4.Flags & ARCHIVE_EMBEDNAME))
          WriteStringLen(fStream, fFoldersTES4[i].Name + "\\" + fFoldersTES4[i].Files[j].Name, false);

        if (fCompress && (aData.size() >= 32)) {
          std::stringstream msData(std::string(aData.begin(), aData.end()));

          // store uncompressed length
          write<Cardinal>(fStream, aData.size());
          // store compressed data
          if (fType == baSSE)
            lz4CompressStream(msData, fStream);
          else
            ZCompressStream(msData, fStream);
          wasCompressed = true;
        }
        else {
          fStream.write(aData.data(), aData.size());
          wasCompressed = false;
        }

        fFoldersTES4[i].Files[j].Size = int64_t(fStream.tellp()) - fFoldersTES4[i].Files[j].Offset;

        // compress flag in Size inverts compression status from the header
        // set it if archive is flagged as compressed but file was not compressed
        if (fCompress && !wasCompressed)
            fFoldersTES4[i].Files[j].Size = fFoldersTES4[i].Files[j].Size | FILE_SIZE_COMPRESS;

        AddPackedData(aData.size(), DataHash, &fFoldersTES4[i].Files[j]);
        break;
      }

      case baFO4: {
        if (SplitDirName(aFileName, fdir, fname) == std::string::npos)
          throw std::runtime_error("File is missing the folder part: " + aFileName);

        SplitNameExt(fname, name, ext);
        if (ext.size() > 0) ext.erase(ext.begin()); // no dot in extension

        i = fHeaderFO4.FileCount;
        ++fHeaderFO4.FileCount;
        // archive2.exe uses /
        fFilesFO4[i].Name = StringReplaceAll(aFileName, '\\', '/');
        fFilesFO4[i].DirHash = CreateHashFO4(fdir);
        fFilesFO4[i].NameHash = CreateHashFO4(name);
        fFilesFO4[i].Ext = Int2Magic(Str2MagicInt(ext));
        fFilesFO4[i].Unknown = iFileFO4Unknown;
        fFilesFO4[i].Offset = fStream.tellp();
        fFilesFO4[i].Size = aData.size();

        if (FindPackedData(aData.size(), DataHash, &fFilesFO4[i]))
          return;

        if (fCompress && (aData.size() >= 32)) {
          std::stringstream msData(std::string(aData.begin(), aData.end()));
          ZCompressStream(msData, fStream);
          fFilesFO4[i].PackedSize = int64_t(fStream.tellp()) - fFilesFO4[i].Offset;
          // rare case: if packed size matches original, then rewrite with original data
          if (fFilesFO4[i].PackedSize == fFilesFO4[i].Size) {
            fFilesFO4[i].PackedSize = 0;
            fStream.seekp(fFilesFO4[i].Offset);
            fStream.write(aData.data(), aData.size());
          }
        }
        else
          fStream.write(aData.data(), aData.size());

        AddPackedData(aData.size(), DataHash, &fFilesFO4[i]);
        break;
      }

      case baFO4dds: {
        if (SplitDirName(aFileName, fdir, fname) == std::string::npos)
          throw std::runtime_error("File is missing the folder part: " + aFileName);

        SplitNameExt(fname, name, ext);
        if (!ext.empty()) ext.erase(ext.begin()); // no dot in extension

        // filling common part
        i = fHeaderFO4.FileCount;
        ++fHeaderFO4.FileCount;
        // archive2.exe uses /
        fFilesFO4[i].Name = StringReplaceAll(aFileName, '\\', '/');
        fFilesFO4[i].DirHash = CreateHashFO4(fdir);
        fFilesFO4[i].NameHash = CreateHashFO4(name);
        fFilesFO4[i].Ext = Int2Magic(Str2MagicInt(ext));
        fFilesFO4[i].UnknownTex = 0;

        // DDS file parameters
        DDSHeader = (PDDSHeader)&aData[0];
        Off = sizeof(*DDSHeader); // offset to image data
        fFilesFO4[i].Width = DDSHeader->dwWidth;
        fFilesFO4[i].Height = DDSHeader->dwHeight;
        fFilesFO4[i].NumMips = DDSHeader->dwMipMapCount;
        // no mipmaps is equal to a single one
        if (fFilesFO4[i].NumMips == 0)
          fFilesFO4[i].NumMips = 1;

        // DXGI detection
        if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_DXT1)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC1_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_DXT3)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC2_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_DXT5)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC3_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_ATI1)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC4_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_BC4U)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC4_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_BC4S)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC4_SNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_ATI2)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC5_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_BC5U)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC5_UNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_BC5S)
          fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_BC5_SNORM);
        else if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_DX10) {
          DDSHeaderDX10 = (PDDSHeaderDX10)&aData[Off];
          Off = Off + sizeof(*DDSHeaderDX10);
          fFilesFO4[i].DXGIFormat = Byte(DDSHeaderDX10->dxgiFormat);
        }
        else {
          if (DDSHeader->ddspf.dwRGBBitCount == 32) {
            if ((DDSHeader->ddspf.dwFlags & DDPF_ALPHAPIXELS) == 0)
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_B8G8R8X8_UNORM);
            else if (DDSHeader->ddspf.dwRBitMask == 0x000000FF)
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_R8G8B8A8_UNORM);
            else
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_B8G8R8A8_UNORM);
          }
          else if (DDSHeader->ddspf.dwRGBBitCount == 16) {
            if ((DDSHeader->ddspf.dwRBitMask == 0xF800) && (DDSHeader->ddspf.dwGBitMask == 0x07E0) &&
                (DDSHeader->ddspf.dwBBitMask == 0x001F) && (DDSHeader->ddspf.dwABitMask == 0x0000))
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_B5G6R5_UNORM);
            else if ((DDSHeader->ddspf.dwRBitMask == 0x7C00) && (DDSHeader->ddspf.dwGBitMask == 0x03E0) &&
                     (DDSHeader->ddspf.dwBBitMask == 0x001F) && (DDSHeader->ddspf.dwABitMask == 0x8000))
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_B5G5R5A1_UNORM);
            else
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_R8G8_UNORM);
          }
          else if (DDSHeader->ddspf.dwRGBBitCount == 8) {
            if (DDSHeader->ddspf.dwFlags && DDPF_ALPHA != 0)
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_A8_UNORM);
            else
              fFilesFO4[i].DXGIFormat = Byte(DXGI_FORMAT_R8_UNORM);
          }
          else
            throw std::runtime_error("Unsupported uncompressed DDS format");
        }

        // MipMap size detection
        switch(TDXGI(fFilesFO4[i].DXGIFormat)) {
          case DXGI_FORMAT_BC1_UNORM: case DXGI_FORMAT_BC1_UNORM_SRGB:
          case DXGI_FORMAT_BC4_UNORM: case DXGI_FORMAT_BC4_SNORM:
            BitsPerPixel = 4;
            break;
          case DXGI_FORMAT_BC2_UNORM: case DXGI_FORMAT_BC2_UNORM_SRGB:
          case DXGI_FORMAT_BC3_UNORM: case DXGI_FORMAT_BC3_UNORM_SRGB:
          case DXGI_FORMAT_BC5_UNORM: case DXGI_FORMAT_BC5_SNORM:
          case DXGI_FORMAT_BC6H_SF16: case DXGI_FORMAT_BC6H_UF16:
          case DXGI_FORMAT_BC7_UNORM: case DXGI_FORMAT_BC7_UNORM_SRGB:
          case DXGI_FORMAT_A8_UNORM:
          case DXGI_FORMAT_R8_SINT: case DXGI_FORMAT_R8_SNORM:
          case DXGI_FORMAT_R8_UINT: case DXGI_FORMAT_R8_UNORM:
            BitsPerPixel = 8;
            break;
          case DXGI_FORMAT_B5G6R5_UNORM: case DXGI_FORMAT_B5G5R5A1_UNORM:
          case DXGI_FORMAT_R8G8_SINT: case DXGI_FORMAT_R8G8_UINT: case DXGI_FORMAT_R8G8_UNORM:
            BitsPerPixel = 16;
            break;
          case DXGI_FORMAT_B8G8R8A8_UNORM: case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
          case DXGI_FORMAT_B8G8R8X8_UNORM: case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
          case DXGI_FORMAT_R8G8B8A8_UNORM: case DXGI_FORMAT_R8G8B8A8_SINT:
          case DXGI_FORMAT_R8G8B8A8_UINT: case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
            BitsPerPixel = 32;
            break;
          default:
            throw std::runtime_error("Unsupported DDS format");
        }
        MipSize = (fFilesFO4[i].Width * fFilesFO4[i].Height * BitsPerPixel) >> 3;

        // cubemaps detection
        fFilesFO4[i].CubeMaps = 0x800;
        if ((DDSHeader->dwCaps2 & DDSCAPS2_CUBEMAP) != 0)
            fFilesFO4[i].CubeMaps = fFilesFO4[i].CubeMaps | 1;

        // number of chunks to store in file record
        DDSInfo.Width = fFilesFO4[i].Width;
        DDSInfo.Height = fFilesFO4[i].Height;
        DDSInfo.MipMaps = fFilesFO4[i].NumMips;
        fFilesFO4[i].TexChunks.resize(GetDDSMipChunkNum(DDSInfo));

        // storing chunks
        for (size_t k = 0; k < fFilesFO4[i].TexChunks.size(); ++k) {
          fFilesFO4[i].TexChunks[k].StartMip = k;
          if (k < fFilesFO4[i].TexChunks.size())
            fFilesFO4[i].TexChunks[k].EndMip = k;
          else {
            // last chunk stores all remaining mipmaps
            fFilesFO4[i].TexChunks[k].EndMip = fFilesFO4[i].NumMips-1;
            MipSize = aData.size() - Off;
          }

          DataHash = CalcDataHash(&aData[Off], MipSize);
          if (!FindPackedData(MipSize, DataHash, &fFilesFO4[i].TexChunks[k])) {
            fFilesFO4[i].TexChunks[k].Offset = fStream.tellp();
            fFilesFO4[i].TexChunks[k].Size = MipSize;
            if (fCompress) {
              std::stringstream msData(std::string(aData.begin(), aData.end()));
              ZCompressStream(msData, fStream);
              fFilesFO4[i].TexChunks[k].PackedSize = int64_t(fStream.tellp()) - fFilesFO4[i].TexChunks[k].Offset;
            }
            else
              fStream.write(&aData[Off], MipSize);

            AddPackedData(MipSize, DataHash, &fFilesFO4[i].TexChunks[k]);
          }
          Off += MipSize;
          MipSize = MipSize / 4;
        }
      }

      case baNone:
      default:
        break;
  }
}

TBytes TwbBSArchive::ExtractFileData(Pointer aFileRecord)
{
  PwbBSFileTES3 FileTES3;
  PwbBSFileTES4 FileTES4;
  PwbBSFileFO4 FileFO4;
  PDDSHeader DDSHeader;
  PDDSHeaderDX10 DDSHeaderDX10;
  Integer size, TexSize;
  bool bCompressed;
  TBytes Buffer, Result;

  if (!(stReading & fStates))
    throw std::runtime_error("Archive is not loaded");

  if (aFileRecord == nullptr)
    return {};

  Files::IStreamPtr streamPtr;
  std::istream* fileStream = nullptr;
  if (fType == baTES3) {
    streamPtr = Files::openConstrainedFileStream(FileName().c_str(), ((Bsa::BSAFile::FileStruct*)aFileRecord)->Offset + fDataOffset, ((Bsa::BSAFile::FileStruct*)aFileRecord)->Size);
  }
  else if (fType != baFO4dds) {
    streamPtr = Files::openConstrainedFileStream(FileName().c_str(), ((Bsa::BSAFile::FileStruct*)aFileRecord)->Offset, ((Bsa::BSAFile::FileStruct*)aFileRecord)->Size);
  }
  else {
    streamPtr = std::make_shared<std::ifstream>(std::ifstream(FileName()));
  }
  fileStream = streamPtr.get();

  switch(fType) {
    case baTES3: {
      FileTES3 = (PwbBSFileTES3)aFileRecord;
      //fileStream->seekg(fDataOffset + FileTES3->Offset, std::ios_base::beg); //already done with openConstrainedFileStream
      Result.resize(FileTES3->Size);
      fileStream->read(Result.data(), Result.size());
      break;
    }
    case baTES4: case baFO3: case baSSE: {
      FileTES4 = (PwbBSFileTES4)aFileRecord;

      //fileStream->seekg(FileTES4->Offset); //already done with openConstrainedFileStream
      size = FileTES4->Size;
      bCompressed = (size & FILE_SIZE_COMPRESS) != 0;
      if (bCompressed)
        size = size & ~FILE_SIZE_COMPRESS;
      if ((fHeaderTES4.Flags & ARCHIVE_COMPRESS) != 0)
        bCompressed = !bCompressed;

      // skip embedded file name + length prefix
      if ((fType == baFO3 || fType == baSSE) && ((fHeaderTES4.Flags & ARCHIVE_EMBEDNAME) != 0))
        size = size - (ReadStringLen(*fileStream, false).size() + 1);

      if (bCompressed) {
        // reading uncompressed size
        Result.resize(read<Cardinal>(*fileStream));
        size -= sizeof(Cardinal);
        if ((Result.size() > 0) && (size > 0)) {
          Buffer.resize(size);
          fileStream->read(Buffer.data(), Buffer.size());
          if (fType == baSSE) {
            lz4DecompressToUserBuf(Buffer.data(), Buffer.size(), Result.data(), Result.size());
          }
          else {
            try {
              DecompressToUserBuf(Buffer.data(), Buffer.size(), Result.data(), Result.size());
            } catch(const std::exception&) {
            // ignore zlib's Buffer error since it happens in vanilla "Fallout - Misc.bsa"
            // Bethesda probably used old buggy zlib version when packing it
            //on E: Exception do if E.Message <> 'Buffer error' then raise;
            }
          }
        }
      }
      else {
        Result.resize(size);
        if (size > 0)
            fileStream->read(Result.data(), size);
      }
      break;
    }

    case baFO4: {
      FileFO4 = (PwbBSFileFO4)aFileRecord;
      //fileStream->seekg(FileFO4->Offset); //already done with openConstrainedFileStream
      if (FileFO4->PackedSize != 0) {
        Buffer.resize(FileFO4->PackedSize);
        fileStream->read(Buffer.data(), Buffer.size());
        Result.resize(FileFO4->Size);
        DecompressToUserBuf(Buffer.data(), Buffer.size(), Result.data(), Result.size());
      }
      else {
        Result.resize(FileFO4->Size);
        fileStream->read(Result.data(), Result.size());
      }
      break;
    }

    case baFO4dds: {
      FileFO4 = (PwbBSFileFO4)aFileRecord;

      TexSize = sizeof(TDDSHeader);
      for (const auto& c : FileFO4->TexChunks)
        TexSize += c.Size;
      Result.resize(TexSize);

      DDSHeader = (PDDSHeader)&Result[0];
      std::copy(MAGIC_DDS.begin(), MAGIC_DDS.end(), DDSHeader->Magic);
      DDSHeader->dwSize = sizeof(TDDSHeader) - MAGIC_DDS.size();
      DDSHeader->dwWidth = FileFO4->Width;
      DDSHeader->dwHeight = FileFO4->Height;
      DDSHeader->dwFlags = DDSD_CAPS | DDSD_PIXELFORMAT |
                           DDSD_WIDTH | DDSD_HEIGHT | DDSD_MIPMAPCOUNT;
      DDSHeader->dwCaps = DDSCAPS_TEXTURE;
      DDSHeader->dwMipMapCount = FileFO4->NumMips;
      if (DDSHeader->dwMipMapCount > 1)
       DDSHeader->dwCaps = DDSHeader->dwCaps | DDSCAPS_MIPMAP | DDSCAPS_COMPLEX;
      DDSHeader->dwDepth = 1;

      DDSHeaderDX10 = (PDDSHeaderDX10 )&Result[sizeof(TDDSHeader)];
      DDSHeaderDX10->resourceDimension = DDS_DIMENSION_TEXTURE2D;
      DDSHeaderDX10->arraySize = 1;

      if (FileFO4->CubeMaps == 2049) {
        DDSHeader->dwCaps = DDSHeader->dwCaps | DDSCAPS2_CUBEMAP | DDSCAPS_COMPLEX
                         | DDSCAPS2_POSITIVEX | DDSCAPS2_NEGATIVEX
                         | DDSCAPS2_POSITIVEY | DDSCAPS2_NEGATIVEY
                         | DDSCAPS2_POSITIVEZ | DDSCAPS2_NEGATIVEZ;
        DDSHeaderDX10->miscFlags = DDS_RESOURCE_MISC_TEXTURECUBE;
      }
      DDSHeader->ddspf.dwSize = sizeof(DDSHeader->ddspf);
      switch(TDXGI(FileFO4->DXGIFormat)) {
        case DXGI_FORMAT_BC1_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DXT1.begin(), MAGIC_DXT1.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height / 2;
          break;
        }
        case DXGI_FORMAT_BC2_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DXT3.begin(), MAGIC_DXT3.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height;
          break;
        }
        case DXGI_FORMAT_BC3_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DXT5.begin(), MAGIC_DXT5.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height;
          break;
        }
        case DXGI_FORMAT_BC4_SNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_BC4S.begin(), MAGIC_BC4S.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height / 2;
          break;
        }
        case DXGI_FORMAT_BC4_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_BC4U.begin(), MAGIC_BC4U.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height / 2;
          break;
        }
        case DXGI_FORMAT_BC5_SNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_BC5S.begin(), MAGIC_BC5S.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height;
          break;
        }
        case DXGI_FORMAT_BC5_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_BC5U.begin(), MAGIC_BC5U.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height;
          break;
        }
        case DXGI_FORMAT_BC1_UNORM_SRGB: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DX10.begin(), MAGIC_DX10.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeaderDX10->dxgiFormat = Integer(FileFO4->DXGIFormat);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height / 2;
          break;
        }
        case DXGI_FORMAT_BC2_UNORM_SRGB: case DXGI_FORMAT_BC3_UNORM_SRGB:
        case DXGI_FORMAT_BC6H_UF16: case DXGI_FORMAT_BC6H_SF16:
        case DXGI_FORMAT_BC7_UNORM: case DXGI_FORMAT_BC7_UNORM_SRGB: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_LINEARSIZE;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DX10.begin(), MAGIC_DX10.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeaderDX10->dxgiFormat = Integer(FileFO4->DXGIFormat);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * FileFO4->Height;
          break;
        }
        case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB: case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
        case DXGI_FORMAT_R8G8B8A8_SINT: case DXGI_FORMAT_R8G8B8A8_UINT: case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DX10.begin(), MAGIC_DX10.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeaderDX10->dxgiFormat = Integer(FileFO4->DXGIFormat);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 4;
          break;
        }
        case DXGI_FORMAT_R8G8_SINT: case DXGI_FORMAT_R8G8_UINT: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DX10.begin(), MAGIC_DX10.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeaderDX10->dxgiFormat = Integer(FileFO4->DXGIFormat);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 2;
          break;
        }
        case DXGI_FORMAT_R8_SINT: case DXGI_FORMAT_R8_SNORM: case DXGI_FORMAT_R8_UINT: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_FOURCC;
          std::copy(MAGIC_DX10.begin(), MAGIC_DX10.end(), DDSHeader->ddspf.dwFourCC);
          DDSHeaderDX10->dxgiFormat = Integer(FileFO4->DXGIFormat);
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width;
          break;
        }
        case DXGI_FORMAT_R8G8B8A8_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_RGB | DDPF_ALPHAPIXELS;
          DDSHeader->ddspf.dwRGBBitCount = 32;
          DDSHeader->ddspf.dwRBitMask = 0x000000FF;
          DDSHeader->ddspf.dwGBitMask = 0x0000FF00;
          DDSHeader->ddspf.dwBBitMask = 0x00FF0000;
          DDSHeader->ddspf.dwABitMask = 0xFF000000;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 4;
          break;
        }
        case DXGI_FORMAT_B8G8R8A8_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_RGB | DDPF_ALPHAPIXELS;
          DDSHeader->ddspf.dwRGBBitCount = 32;
          DDSHeader->ddspf.dwRBitMask = 0x00FF0000;
          DDSHeader->ddspf.dwGBitMask = 0x0000FF00;
          DDSHeader->ddspf.dwBBitMask = 0x000000FF;
          DDSHeader->ddspf.dwABitMask = 0xFF000000;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 4;
          break;
        }
        case DXGI_FORMAT_B8G8R8X8_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_RGB;
          DDSHeader->ddspf.dwRGBBitCount = 32;
          DDSHeader->ddspf.dwRBitMask = 0x00FF0000;
          DDSHeader->ddspf.dwGBitMask = 0x0000FF00;
          DDSHeader->ddspf.dwBBitMask = 0x000000FF;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 4;
          break;
        }
        case DXGI_FORMAT_B5G6R5_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_RGB;
          DDSHeader->ddspf.dwRGBBitCount = 16;
          DDSHeader->ddspf.dwRBitMask = 0x0000F800;
          DDSHeader->ddspf.dwGBitMask = 0x000007E0;
          DDSHeader->ddspf.dwBBitMask = 0x0000001F;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 2;
          break;
        }
        case DXGI_FORMAT_B5G5R5A1_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_RGB | DDPF_ALPHAPIXELS;
          DDSHeader->ddspf.dwRGBBitCount = 16;
          DDSHeader->ddspf.dwRBitMask = 0x00007C00;
          DDSHeader->ddspf.dwGBitMask = 0x000003E0;
          DDSHeader->ddspf.dwBBitMask = 0x0000001F;
          DDSHeader->ddspf.dwABitMask = 0x00008000;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 2;
          break;
        }
        case DXGI_FORMAT_R8G8_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_LUMINANCE | DDPF_ALPHAPIXELS;
          DDSHeader->ddspf.dwRGBBitCount = 16;
          DDSHeader->ddspf.dwRBitMask = 0x000000FF;
          DDSHeader->ddspf.dwABitMask = 0x0000FF00;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width * 2;
          break;
        }
        case DXGI_FORMAT_A8_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_ALPHA;
          DDSHeader->ddspf.dwRGBBitCount = 8;
          DDSHeader->ddspf.dwABitMask = 0x000000FF;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width;
          break;
        }
        case DXGI_FORMAT_R8_UNORM: {
          DDSHeader->dwFlags = DDSHeader->dwFlags | DDSD_PITCH;
          DDSHeader->ddspf.dwFlags = DDPF_LUMINANCE;
          DDSHeader->ddspf.dwRGBBitCount = 8;
          DDSHeader->ddspf.dwRBitMask = 0x000000FF;
          DDSHeader->dwPitchOrLinearSize = FileFO4->Width;
          break;
        }
        default:
          break;
      }
      TexSize = sizeof(TDDSHeader);
      if (to_array(DDSHeader->ddspf.dwFourCC) == MAGIC_DX10) {
        Result.resize(Result.size() + sizeof(TDDSHeaderDX10));
        TexSize += sizeof(TDDSHeaderDX10);
      }
      // append chunks
      for (const auto& c : FileFO4->TexChunks) {
        fileStream->seekg(c.Offset);
        // compressed chunk
        if (c.PackedSize != 0) {
          Buffer.resize(c.PackedSize);
          fileStream->read(&Buffer[0], Buffer.size());
          DecompressToUserBuf(&Buffer[0], Buffer.size(), &Result[TexSize], c.Size);
        }
        // uncompressed chunk
        else
          fileStream->read(&Result[TexSize], c.Size);
        TexSize += c.Size;
      }
    }

    default:
      throw std::runtime_error("Extraction is not supported for this archive");
  }
  return Result;
}

TBytes TwbBSArchive::ExtractFileData(const std::string& aFileName)
{
  if (!(stReading & fStates))
    throw std::runtime_error("Archive is not loaded");

  Pointer FileRecord = FindFileRecord(aFileName);

  if (FileRecord == nullptr)
    throw std::runtime_error("File not found in archive");

  return ExtractFileData(FileRecord);
}

void TwbBSArchive::ExtractFileToStream(const std::string& aFileName, std::ostream& aStream)
{
  TBytes FileData = ExtractFileData(aFileName);
  aStream.write(FileData.data(), FileData.size());
}

void TwbBSArchive::ExtractFile(const std::string& aFileName, const std::string& aSaveAs)
{
  if (!(stReading & fStates))
    throw std::runtime_error("Archive is not loaded");

  std::ofstream fs(aSaveAs, std::ios_base::binary);
  ExtractFileToStream(aFileName, fs);
}

void TwbBSArchive::IterateFiles(const TBSFileIterationProc& aProc)
{
  if (!aProc)
    return;

  switch(fType) {
    case baTES3: {
      for (const auto& f : fFilesTES3)
        if (aProc(*this, f.Name, (Pointer)&f, nullptr))
          break;
      break;
    }
    case baTES4: case baFO3: case baSSE: {
      for (const auto& d : fFoldersTES4)
        for (const auto& f : d.Files)
          if (aProc(*this, d.Name + "\\" + f.Name, (Pointer)&f, (Pointer)&d))
            break;
      break;
    }
    case baFO4: case baFO4dds: {
      for (const auto& f : fFilesFO4)
        if (aProc(*this, f.Name, (Pointer)&f, nullptr))
          break;
      break;
    }
    case baNone:
    default:
      break;
  }
}
/*
{procedure TwbBSArchive::IterateFolders(aProc: TBSFileIterationProc);
var
  i: Integer;
{
  if not Assigned(aProc) then
    Exit;

  if fType in [baTES4, baFO3, baSSE] then
  for i := Low(fFoldersTES4) to High(fFoldersTES4) do
    aProc(Self, fFoldersTES4[i].Name, nil, @fFoldersTES4[i]);
end;}
*/
bool TwbBSArchive::FileExists(const std::string& aFileName)
{
  return FindFileRecord(aFileName) != nullptr;
}

void TwbBSArchive::Close()
{
  fStream.close();
  if (stWriting & fStates)
    std::filesystem::remove(fFileName);

  fStates = 0;
  fType = baNone;
  fFileName = "";
  fDataOffset = 0;
  fHeaderTES3 = {};
  fFilesTES3.clear();
  fHeaderTES4= {};
  fFoldersTES4.clear();
  fHeaderFO4 = {};
  fFilesFO4.clear();

  if (fShareData) {
    fPackedData.clear();
  }
}

std::string ExcludeTrailingPathDelimiter(std::string folder)
{
  if (!folder.empty() && folder.back() == '/')
    folder.pop_back();
  return folder;
}

void TwbBSArchive::ResourceList(std::vector<std::string>& aList, const std::string& aFolder) const
{
  std::string Folder = ExcludeTrailingPathDelimiter(aFolder);
  switch(fType) {
    case baTES3: {
      for (const auto& f : fFilesTES3)
        if (Folder.empty() || f.Name.rfind(Folder, 0) == 0)
          aList.push_back(f.Name);
      break;
    }
    case baTES4: case baFO3: case baSSE: {
      for (const auto& dir : fFoldersTES4)
        if ((Folder.empty()) || dir.Name.rfind(Folder) == 0)
          for (const auto& f : dir.Files)
            aList.push_back(dir.Name + "\\" + f.Name);
      break;
    }
    case baFO4: case baFO4dds: {
      for (const auto& f : fFilesFO4)
        if (Folder.empty() || f.Name.rfind(Folder) == 0)
          aList.push_back(f.Name);
      break;
    }
    case baNone:
    default:
      break;
  }
}

Bsa::BSAFile::FileList TwbBSArchive::getList() const
{
  Bsa::BSAFile::FileList result;
  switch(fType) {
    case baTES3: {
      for (const auto& f : fFilesTES3)
        result.push_back(&f);
      break;
    }
    case baTES4: case baFO3: case baSSE: {
      for (const auto& dir : fFoldersTES4)
        for (const auto& f : dir.Files)
          result.push_back(&f);
      break;
    }
    case baFO4: case baFO4dds: {
      for (const auto& f : fFilesFO4)
        result.push_back(&f);
      break;
    }
    case baNone:
    default:
      break;
  }
  return result;
}
/*
void TwbBSArchive::ResolveHash(const aHash: UInt64; var Results: TArray<string>);
var
  Len  : Integer;
  i, j : Integer;
{
  case fType of
    baTES3:
      for i := Low(fFilesTES3) to High(fFilesTES3) do
        with fFilesTES3[i] do
          if aHash = Hash then {
            SetLength(Results, Succ(Len));
            Results[Len] := Name;
            Inc(Len);
          }
    baTES4, baFO3, baSSE:
      for i := Low(fFoldersTES4) to High(fFoldersTES4) do
        with fFoldersTES4[i] do {
          if aHash = Hash then {
            SetLength(Results, Succ(Len));
            Results[Len] := Name;
            Inc(Len);
          }
          for j := Low(Files) to High(Files) do
            with Files[j] do
              if aHash = Hash then {
                SetLength(Results, Succ(Len));
                Results[Len] := Name;
                Inc(Len);
              }
        }
    {
    baFO4, baFO4dds:
      for i := Low(fFilesFO4) to High(fFilesFO4) do
        with fFilesFO4[i] do {
          if aHash = NameHash then {
            SetLength(Results, Succ(Len));
            Results[Len] := Name;
            Inc(Len);
          }
        }
    }
  }
}*/
}
