variables:
  PACKAGE_REGISTRY_URL:
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/bsatool2/${CI_COMMIT_TAG}"
  PACKAGE_LINUX_AMD64: "bsatool2-${CI_COMMIT_TAG}-linux-amd64.zip"
  PACKAGE_WIN_AMD64: "bsatool2-${CI_COMMIT_TAG}-windows-amd64.zip"
  PACKAGE_DARWIN_AMD64: "bsatool2-${CI_COMMIT_TAG}-darwin-amd64.zip"

stages:
- build
- test
- upload
- release

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml

build:
  stage: build
  image: alpine:latest
  cache:
    paths:
    - build
  before_script:
    - apk add --no-cache lz4-dev lz4-static make cmake binutils gcc g++ bzip2-dev bzip2-static zlib-static zlib-dev git openssl-dev openssl-libs-static zip
  script:
    - mkdir build -p
    - cd build
    - cmake -D CMAKE_EXE_LINKER_FLAGS="-static -Os"
            -D CMAKE_MAKE_PROGRAM=make
            -D LZ4_LIBRARY=/usr/lib/liblz4.a
            -D CMAKE_BUILD_TYPE=Release
            -D OPENSSL_USE_STATIC_LIBS=TRUE
            -D OPENSSL_SSL_LIBRARY=/usr/lib/libssl.a
            -D OPENSSL_CRYPTO_LIBRARY=/usr/lib/libcrypto.a
            ..
    - make
    - strip bsatool2
    - cd ..
    - mv build/bsatool2 bsatool2
    - zip ${PACKAGE_LINUX_AMD64} bsatool2 README.md LICENSE
  artifacts:
    paths:
      - ${PACKAGE_LINUX_AMD64}

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  needs:
    - job: build
      artifacts: true
    - job: windows_build
      artifacts: true
  script: >
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "$PACKAGE_LINUX_AMD64"
      "${PACKAGE_REGISTRY_URL}/${PACKAGE_LINUX_AMD64}"

      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "$PACKAGE_WIN_AMD64"
      "${PACKAGE_REGISTRY_URL}/${PACKAGE_WIN_AMD64}"

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: build
      artifacts: true
    - job: windows_build
      artifacts: true
  only:
    - tags
  script:
    - echo 'running release_job for $CI_COMMIT_TAG'
    - >
      release-cli create --name "Release $CI_COMMIT_TAG"
      --tag-name $CI_COMMIT_TAG
      --ref "$CI_COMMIT_SHA"
      --assets-link "{\"name\":\"${PACKAGE_LINUX_AMD64}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_LINUX_AMD64}\", \"link_type\": \"package\", \"filepath\": \"/${PACKAGE_LINUX_AMD64}\"}"
      --assets-link "{\"name\":\"${PACKAGE_WIN_AMD64}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_WIN_AMD64}\", \"link_type\": \"package\", \"filepath\": \"/${PACKAGE_WIN_AMD64}\"}"

windows_build:
  stage: build
  tags:
    - windows
  before_script:
    - Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
    - choco install git --force --params "/GitAndUnixToolsOnPath" -y
    - choco install cmake.install --installargs 'ADD_CMAKE_TO_PATH=System' -y
    - choco install openssl -y
    - choco install 7zip -y
    - refreshenv
    - iwr https://github.com/madler/zlib/archive/refs/tags/v1.2.12.zip -OutFile zlib-1.2.12.zip
    - 7z x -y zlib-1.2.12.zip
    - cd zlib-1.2.12
    - cmake .
    - cmake --build . --config Release
    - cd ..
    - iwr https://github.com/lz4/lz4/archive/refs/tags/v1.9.4.zip -OutFile lz4-1.9.4.zip
    - 7z x -y lz4-1.9.4.zip
    - cd lz4-1.9.4/build/cmake
    - cmake
      -D BUILD_STATIC_LIBS=ON
      -D LZ4_BUILD_CLI=OFF
      -D LZ4_BUILD_LEGACY_LZ4C=OFF
      .
    - cmake --build . --config Release
    - cd ../../..
  script:
    - mkdir build
    - cd build
    - cmake
        -D LZ4_LIBRARY=../lz4-1.9.4/build/cmake/Release/lz4_static.lib
        -D LZ4_INCLUDE_DIR=../lz4-1.9.4/lib/
        -D LZ4_VERSION=1.9.4
        -D ZLIB_LIBRARY=../zlib-1.2.12/Release/zlibstatic
        -D ZLIB_INCLUDE_DIR=../zlib-1.2.12
        -D OPENSSL_USE_STATIC_LIBS=TRUE
        ..
    - cmake --build . --config Release
    - cd ..
    - mv build/Release/bsatool2.exe bsatool2.exe
    - 7z a -tzip ${PACKAGE_WIN_AMD64} bsatool2.exe README.md LICENSE
  artifacts:
    paths:
      - ${PACKAGE_WIN_AMD64}
