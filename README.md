# bsatool2

This is a version of OpenMW's bsatool which supports the creation of bsa files from the later Bethesda games.

Extracted from https://gitlab.com/OpenMW/openmw/-/merge_requests/783.

## Building

```bash
mkdir build
cd build
# By default installs into /usr/local/bin
cmake -DCMAKE_BUILD_TYPE=Release
# OR
# You could install into ~/.local/bin which doesn't require superuser permissions
cmake -DCMAKE_INSTALL_PREFIX=~/.local -DCMAKE_BUILD_TYPE=Release

make
# May require superuser permissions depending on install location
make install
```
